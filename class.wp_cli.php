<?php

/** 
 * @file
 * @brief This file includes the Closings_WP_CLI_Command class
 * 
 * This includes methods for accessing the closings plugin via wp-cli
 */

/**
 * List and manipulate closings
 *
 * ## OPTIONS
 *
 * ## EXAMPLES
 *
 * wp closings list
 */
class Closings_WP_CLI_Command extends WP_CLI_Command {

	public function __construct ( ) {
		/** @ignore Disable caches from W3 Total Cache. CLI is not considered within wp_admin. */
		if ( ! defined('DONOTCACHEDB') ) define('DONOTCACHEDB', true);
        $this->closings = Closings::get_instance();
	}
	
	/**
	 * List closings
	 *
	 * ## OPTIONS
     *
     * <org_slug>
     * : The slug(s) of the closing organization
     *
     * [--format=<format>]
     * : Accepted values: table, csv, json, count. Default: table
     *
     * [--fields=<fields>]
     * : Limit to specific fields. Defaults to org_name,status,note,dayofweek
     * May include org_name,status,note,dayofweek,start,end and any WP_Post fields
	 *
	 * ## EXAMPLES
	 *
	 * wp closings list springfield-isd
	 *
	 * @subcommand list
	 * @synopsis [<org_slug>...] [--format=<format>] [--fields=<fields>]
	 */
	public function list_closings ($args, $assoc_args) {
        $fields = isset($assoc_args['fields']) ? explode(',', $assoc_args['fields'])
            : array(
                'org_name',
                'status',
                'note',
                'dayofweek'
            );
        $formatter = new WP_CLI\Formatter($assoc_args, $fields, null);

        $query_args = array();
        if (count($args)) {
            $query_args['closing_org'] = implode(',', $args);
        }
        $closings = $this->closings->get_closings($query_args);
        foreach($closings as &$cl) {
            $cl->org_name = $cl->org->name;
        }
        
        $formatter->display_items($closings);
    }

    /**
     * List organizations
     *
     * ## OPTIONS
     *
     * [--format=<format>]
     * : Accepted values: table, csv, json, count. Default: table
     *
     * [--fields=<fields>]
     * : Limit to specific fields. Defaults to name,category,city,state,phone
     * May include name, category, street_address, city, state, zip, phone, phone2, phone3, website and any taxonomy fields
	 *
     * ## EXAMPLES
     * 
     * @subcommand list-orgs
     * @alias list-organizations
	 * @synopsis [--format=<format>] [--fields=<fields>]
     */
    public function list_orgs($args, $assoc_args) {
        $fields = isset($assoc_args['fields']) ? explode(',', $assoc_args['fields'])
            : array(
                'name',
                'category',
                'city',
                'state',
                //'phone' // TODO
            );
        $formatter = new WP_CLI\Formatter($assoc_args, $fields, null);

        $orgs = $this->closings->get_orgs();
        $formatter->display_items($orgs);
    }

    /**
     * Import from CSV
     *
     * Your CSV file must include a header row, with, at a minimum the following columns:
     * email, org_name
     *
     * It may also include:
     * *user_login*, *pass*, first_name, last_name, street_address, city, state, zip, *category*, phone, phone2, phone3, website
     *
     * If no user_login is included, the domain portain of the email will be used. 
     * If no password is included, a random password will be generated. 
     * The category should match one of the category names. 
     * If no category is chosen, or an invalid category is chosen, "Other" will be chosen.
     *
     * ## OPTIONS
     *
     * file
     * : CSV file to import, or - for stdin
     *
     * ## EXAMPLES
     * 
     * wp closings import /path/to/file.csv
     *
     * @synopsis <file>...
     */
    public function import($args, $assoc_args) {
        foreach($args as $file) {
            if ($file == "-") $file = "php://stdin";
            $new_orgs = $this->closings->import($file);
            if (is_wp_error($new_orgs)) {
                WP_CLI::warning(__("There were errors processing your file.", 'closings'));
                foreach($new_orgs->get_error_codes() as $error_code) {
                    WP_CLI::warning($new_orgs->get_error_message($error_code));
                    
                }
            } else {
                WP_CLI::line(sprintf(__("Successfully imported %s.", 'closings'), $file));
                foreach($new_orgs as $org) {
                    WP_CLI::line("===============");
                    if (is_wp_error($org['return'])) {
                        WP_CLI::warning(sprintf(__("Failed to properly add %s.", 'closings'), $org['org_name']));
                        foreach($org['return']->get_error_codes() as $error_code) {
                            WP_CLI::warning($org['return']->get_error_message($error_code));
                        }
                    } else {
                        WP_CLI::line(__("Successfully added organization", 'closings') . ": " . $org['org_name']);
                        WP_CLI::line(admin_url('edit-tags.php?action=edit&taxonomy=closing_org&post_type=closing&tag_ID=' . $org['return']['org']->term_id));
                        WP_CLI::line(sprintf(__("Login created for %s.", 'closings'), $org['user_login']));
                    }
                    WP_CLI::line("");
                }
            }
        }
    }

    /**
     * Export to CSV
     *
     * ## OPTIONS
     *
     * file
     * : CSV file to which to export, or - for stdout
     *
     * ## EXAMPLES
     *
     * wp closings import /path/to/file.csv
     *
     * @synopsis <file>
     */
    public function export($args, $assoc_args) {
        $file = $args[0] == "-" ? "php://stdout" : $args[0];
        $out = fopen($file, "w");
        if (!$out) WP_CLI::error("Unable to write to {$args[0]}");

        $this->closings->export($out);
        if ($file != "php://stdout") WP_CLI::line("CSV file written to $file");
    }

    // TODO: list-users [admin/newsroom/user/all]
    // TODO: make-user user org-id/org-slug, make-newsroom user, make-admin user
    // TODO: close-org org-id/org-slug day status [note]
    // TODO: list-closing-types
}
WP_CLI::add_command('closings', 'Closings_WP_CLI_Command');
