<?php

/** @file */

/**
 * ClosingsImportIterator class
 *
 * There's no reason to interact with this class directly.
 * It can be instantiated from Closings::import(), and used as any Iterator
 * E.g., the following code snippet is useful:
 *
 *     $orgs = closings_object()->import($file);
 *     foreach($orgs as $org) do_something($org);
 * @see Closings::import()
 */
class ClosingsImportIterator implements Iterator {

    private $position;
    private $handle;
    private $row;
    private $header;
    private $c;

    /**
     * Constructor
     */
    public function __construct($file) {
        if ($file != "php://stdin") {
            if (!is_file($file)) throw new ClosingsIteratorException(__("Invalid file"), 'invalid_file');
            if (!is_readable($file)) throw new ClosingsIteratorException(__("File not readable"), 'unreadable_file');
        }

        $this->c = Closings::get_instance();
        $this->handle = fopen($file, "r");
        $this->rewind();
    }

    public function __destruct() {
        fclose($this->handle);
    }

    public function rewind() {
        rewind($this->handle);

        $header = fgetcsv($this->handle);
        $have_headers = in_array("email", $header) && in_array("org_name", $header);
        if (!$have_headers) throw new ClosingsIteratorException(__("Invalid/missing column headers", 'closings'), 'invalid_headers');

        $this->header = array_flip($header);
        $this->row = null;
        $this->position = -1;

        $this->next();
    }

    public function next() {
        $this->row = fgetcsv($this->handle);
        ++$this->position;
    }

    public function key() {
        return $this->position;
    }

    public function valid() {
        return !empty($this->row);
    }

    /**
     * Returns the most recently imported organization
     *
     * @see Closings::add_new_org()
     * @return array|WP_Error
     */
    public function current() {
        $args = array();
        $args['email'] = $this->row[$this->header['email']];
        $args['org_name'] = $this->row[$this->header['org_name']];

                        
        if (isset($this->header['user_login']) && !empty($this->row[$this->header['user_login']])) {
            $args['user_login'] = strtolower(trim($this->row[$this->header['user_login']]));
            $args['user_login_set'] = "From row";
        } else {
            $ex = explode("@", $args['email']);
            $args['user_login'] = $ex[0];
            $args['user_login_set'] = "From email";
        }
        if (isset($this->header['pass']) && !empty($this->row[$this->header['pass']])) {
            $args['pass1'] = $args['pass2'] = $this->row[$this->header['pass']];
        } else {
            $args['pass1'] = $args['pass2'] = wp_generate_password();
        }

        if (isset($this->header['category'])) {
            $cats = array_flip($this->c->get_org_categories());
            $args['category'] =
                !empty($cats[$this->row[$this->header['category']]])
                ? $cats[$this->row[$this->header['category']]]
                : "cat_X";
            $args['category_orig'] = $this->row[$this->header['category']];
        } else {
            $args['category'] = "cat_X";
            $args['category_orig'] = false;
        }

        if (!empty($this->header['first_name'])) $args['first_name'] = $this->row[$this->header['first_name']];
        if (!empty($this->header['last_name'])) $args['last_name'] = $this->row[$this->header['last_name']];
        if (!empty($this->header['phone'])) $args['phone'] = $this->row[$this->header['phone']];
        if (!empty($this->header['alt_name'])) $args['alt_name'] = $this->row[$this->header['alt_name']];
        if (!empty($this->header['alt_email'])) $args['alt_email'] = $this->row[$this->header['alt_email']];
        if (!empty($this->header['alt_phone'])) $args['alt_phone'] = $this->row[$this->header['alt_phone']];
        if (!empty($this->header['street_address'])) $args['street_address'] = $this->row[$this->header['street_address']];
        if (!empty($this->header['city'])) $args['city'] = $this->row[$this->header['city']];
        if (!empty($this->header['state'])) $args['state'] = $this->row[$this->header['state']];
        if (!empty($this->header['zip'])) $args['zip'] = $this->row[$this->header['zip']];
        if (!empty($this->header['website'])) $args['website'] = $this->row[$this->header['website']];
        if (!empty($this->header['notes'])) $args['notes'] = $this->row[$this->header['notes']];

        $r = $this->c->add_new_org($args);

        /*
         * Fires after a new organization is imported
         * Useful for performing post-add actions, such as emailing the new user
         *
         * @param array|WP_Error $r if add was successful, an array with the WP_User and taxonomy, else a WP_Error
         */
        do_action('closings_import_org', $r);
        $args['return'] = $r;
        return $args;
    }

}

class ClosingsIteratorException extends Exception {
    public function __construct($message, $code) {
        $this->message = $message;
        $this->code = $code;
    }
}