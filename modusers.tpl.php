<?php
function sort_users($a, $b) {
	return strcmp($a->display_name, $b->display_name);
}
?>
<div class="wrap">
<h2><?php _e('Modify Users','closings'); ?></h2>
<?php if (is_array($notices)): foreach($notices as $n): ?>
	<?php if (empty($n['class'])) $n['class'] = ''; ?>
	<?php if (empty($n['text'])) $n['text'] = ''; ?>
	<div class="notice is-dismissible below-h2 <?php echo esc_attr($n['class']); ?>?>"><p>
		<?php echo $n['text']; ?>
	</p></div>
<?php endforeach; endif; ?>

<?php echo $pages_out; ?>

<div class="org-user-type-group">
<h3><?php _e('Closing Admin Users','closings'); ?></h3>
<?php if ($users['admins']): ?><ul class="ul-disc">
<?php foreach($users['admins'] as $user): ?>
<li class="user user-admin" id="user-<?php echo $user->ID; ?>">
	<?php echo $user->display_name; ?>
	<a href="<?php echo wp_nonce_url(add_query_arg(array('show'=>'remove','type'=>'admin','user'=>$user->ID)),'del_org_admin','_wpnonce_del_org_admin'); ?>" class="add-new-h2">Remove</a>
</li>
<?php endforeach; ?></ul>
<?php endif; ?>
<h4><?php _e('Add New','closings'); ?></h4>
<form method="get">
  <?php wp_nonce_field('add_org_admin', '_wpnonce_add_org_admin'); ?>
  <input type="hidden" name="page" value="closings-admin" />
  <input type="hidden" name="show" value="add" />
  <input type="hidden" name="type" value="admin" />
  <table class="form-table"><tbody>
	  <tr class="form-field">
		<th scope="row"><label for="add-admin"><?php _e('Username'); ?></label></th>
		<td><select name="user" id="add-admin">
			<?php $theseusers = array_merge($users['non_users'], $users['newsroom']); usort($theseusers, 'sort_users'); ?>
			<?php foreach($theseusers as $user): ?>
				<option value="<?php echo $user->ID; ?>"><?php echo $user->display_name; ?></option>
			<?php endforeach; ?>
		</select></td>
	  </tr>
  </tbody></table>
  <?php submit_button(__('Add Closings Admin')); ?>
</form>
</div>

<div class="org-user-type-group">
<h3><?php _e('Closing Newsroom Users','closings'); ?></h3>
<?php if ($users['newsroom']): ?><ul class="ul-disc">
<?php foreach($users['newsroom'] as $user): ?>
<li class="user user-newsroom" id="user-<?php echo $user->ID; ?>">
    <?php echo $user->display_name; ?>
    <a href="<?php echo wp_nonce_url(add_query_arg(array('show'=>'remove','type'=>'newsroom','user'=>$user->ID)),'del_org_newsroom','_wpnonce_del_org_newsroom'); ?>" class="add-new-h2">Remove</a>
</li>
<?php endforeach; ?></ul>
<?php endif; ?>
<h4><?php _e('Add New','closings'); ?></h4>
<form method="get">
  <?php wp_nonce_field('add_org_newsroom', '_wpnonce_add_org_newsroom'); ?>
  <input type="hidden" name="page" value="closings-admin" />
  <input type="hidden" name="show" value="add" />
  <input type="hidden" name="type" value="newsroom" />
  <table class="form-table"><tbody>
	  <tr class="form-field">
		<th scope="row"><label for="add-newsroom"><?php _e('Username'); ?></label></th>
		<td><select name="user" id="add-newsroom">
			<?php $theseusers = array_merge($users['non_users']); usort($theseusers, 'sort_users'); ?>
			<?php foreach($theseusers as $user): ?>
				<option value="<?php echo $user->ID; ?>"><?php echo $user->display_name; ?></option>
			<?php endforeach; ?>
		</select></td>
	  </tr>
  </tbody></table>
  <?php submit_button(__('Add Closings Newsroom User')); ?>
</form>
</div>

<div class="org-user-type-group">
<h3><?php _e('Closing Users','closings'); ?></h3>
<?php if ($users['users']): ?><ul class="ul-disc">
<?php foreach($users['users'] as $user): ?>
<li class="user user-users" id="user-<?php echo $user->ID; ?>">
	<?php echo $user->display_name; ?>
	(<?php echo $user->org->name; ?>)
	<a href="<?php echo wp_nonce_url(add_query_arg(array('show'=>'remove','type'=>'user','user'=>$user->ID)),'del_org_user','_wpnonce_del_org_user'); ?>" class="add-new-h2">Remove</a>
</li>
<?php endforeach; ?></ul>
<?php endif; ?>
<h4><?php _e('Add New','closings'); ?></h4>
<form method="get">
  <?php wp_nonce_field('add_org_user', '_wpnonce_add_org_user'); ?>
  <input type="hidden" name="page" value="closings-admin" />
  <input type="hidden" name="show" value="add" />
  <input type="hidden" name="type" value="user" />
  <table class="form-table"><tbody>
	  <tr class="form-field">
		<th scope="row"><label for="add-user"><?php _e('Username'); ?></label></th>
		<td><select name="user" id="add-user">
			<?php $theseusers = array_merge($users['non_users']); ?>
			<?php foreach($users['users'] as $user) if (empty($user->org->name)) $theseusers[] = $user; ?>
			<?php usort($theseusers, 'sort_users'); ?>
			<?php foreach($theseusers as $user): ?>
				<option value="<?php echo $user->ID; ?>"><?php echo $user->display_name; ?></option>
			<?php endforeach; ?>
		</select></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="org"><?php _e('Organizaion', 'closings'); ?></label></th>
		<td><select name="org" id="org">
			<?php foreach($orgs as $org): ?>
			<option value="<?php echo $org->term_taxonomy_id; ?>"><?php echo $org->name; ?></option>
			<?php endforeach; ?>
		</select></td>
	  </tr>
  </tbody></table>
  <?php submit_button(__('Add Closings User')); ?>
</form>
</div>

</div>
