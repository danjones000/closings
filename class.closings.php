<?php

/** 
 * @file
 * @brief This file includes the Closings class
 */

require_once('class.closings_import_iterator.php');

/**
 * @brief The workhorse of the `closings` plugin
 *
 * This class does most of the work of the plugin.<br>
 * The Closings::init() method is called on Wordpress `init`, which, along with the constructor, 
 * sets up all the necessary plugin framework.
 *
 * To access the functionality of this plugin from themes and other plugins, simply call the 
 * necessary method from an instance of this class.
 *
 * E.g., try the follow code to get an array of the active closings in the system:
 *
 *     $c = Closings::get_instance();
 *     $current = $c->get_closings();
 */
class Closings {

    /**
     * record if initialized
     *
     * @static
     * @var bool
     */
    private static $initiated = false;

    /**
     * Singleton instance
     *
     * @static
     * @var Closings
     */
    private static $_instance = false;

    /**
     * Closing Post Type
     *
     * @var stdClass post type
     */
    public $post_type = null;

    /**
     * Closing Org Taxonomy
     *
     * @var stdClass post type
     */
    public $taxonomy = null;

    /**
     * Closing Org Taxonomy prefix
     *
     * @var string
     */
    public $prefix = 'closing_org_';

    /**
     * Timezone used for datetime functions
     */
    private $timezone;
    private $default_timezone;

    /**
     * Singleton function<br>
     * Returns an instance of Closings
     *
     * @static
     * @return Closings
     */
    public static function get_instance() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Constructor
     */
    private function __construct ( ) {
        $tz = get_option('timezone_string');
        $gmoff = get_option('gmt_offset');
        $gmoff = $gmoff < 0 ? "Etc/GMT" . $gmoff : ($gmoff > 0 ? "Etc/GMT+" . $gmoff : "UTC");
        $this->timezone = $tz ?: $gmoff;
        $this->default_timezone = date_default_timezone_get();

        // Activation and deactivation hooks
        register_activation_hook(CLOSINGS_PLUGIN, array($this, 'activate_plugin'));
        register_deactivation_hook(CLOSINGS_PLUGIN, array($this, 'deactivate_plugin'));

        add_action('init', array($this, 'init'));                      // Initialize plugin
        add_action('admin_menu', array($this, "add_admin_menus"), 9);  // Menus
        add_action('add_meta_boxes', array($this, "add_form_script")); // Add javascript for form
        add_action('edit_form_after_title', array($this, "add_form")); // Closing post type options
        add_action('save_post_closing', array($this, "save_closing"), 1); // Save post hook
        add_action('admin_init', array($this, "admin_init"));          // For admin stylesheet
        add_action('wp_ajax_import_closings', array($this, "ajax_import"));
        add_action('trashed_post', array($this, "redirect_after_trash")); // To redirect after a closing is trashed
        add_action('closing_org_edit_form_fields', array($this, "org_link_to_user"));

        // To customize admin for Closing User
        add_action('wp_before_admin_bar_render', array($this, 'remove_home_button'));
        add_action('admin_head', array($this, 'remove_dashboard'));

        add_action('show_user_profile', array($this, "user_fields"));
        add_action('edit_user_profile', array($this, "user_fields"));
        add_action('personal_options_update', array($this, "user_fields_update"));
        add_action('edit_user_profile_update', array($this, "user_fields_update"));

        // Filters
        add_filter('wp_insert_post_data', array($this,'closing_save_filter'), 10, 2); // Filter for saving post
        add_filter('login_redirect', array($this, "redirect_user_login"), 10, 3); // For redirecting users to closing screen
        add_filter('user_has_cap', array($this, "has_cap"), 5, 4);
        add_filter('redirect_post_location', array($this, "redirect_after_save"), 10, 2);

        // cron action 
        add_action('closings_expire', array($this, "expire_closings"));

        // shortcodes
        add_shortcode('closings', array($this, 'closings_shortcode'));
        add_shortcode('closings_by_state', array($this, 'closings_by_state_shortcode'));
    }

    /** @defgroup wphooks Wordpress Filters and Actions
     *  @{ 
     */

    /**
     * plugin activation hook
     *
     * @return bool
     */
    public function activate_plugin() {
        // Add user role
        add_role('closings-user'
                 , __('Closings User','closings')
                 , array('read' => true, 'close_school' => true, 'subscriber' => true, 'level_0' => true));

        // Add capabilities to admin users
        $users = get_users(array('role' => 'administrator'));
        foreach($users as $u) {
            $this->admin_privileges($u);
        }

        // Create cron job
        wp_schedule_event(time(), 'hourly', 'closings_expire');

        return true;
    }

    /**
     * plugin deactivation hook
     * 
     * currently does nothing
     *
     * @return bool
     */
    public function deactivate_plugin() {
        // Remove cron
        $ts = wp_next_scheduled('closings_expire');
        wp_unschedule_event($ts, 'closings_expire');

        // Remove user role
        remove_role('closings-user');

        return true;
    }

    /**
     * plugin initialization<br>
     * registers types, taxonomies, themes, etc.
     *
     * @see Closings::add_closing_post_type()
     * @see Closings::add_closing_org()
     * @see Closings::add_closing_status()
     * @return bool false if previously initiated, true otherwise
     */
    public function init() {
        if (self::$initiated) return false;
        self::$initiated = true;

        /*
         * Fires before closings init
         *
         * @var before_closings_init
         * @param Closings $this The Closings instance
         */
        do_action('before_closings_init', $this);

        // Register styles and scripts
        wp_register_style('closing_admin_style', plugins_url('style.css', __FILE__));
        wp_register_style('closing_admin_user_style', plugins_url('style_user.css', __FILE__));
        wp_register_script('closings_post_script', plugins_url('closings.postscript.min.js', __FILE__), array('post'));

        // Add post types and organizations
        $this->add_closing_post_type();
        $this->add_closing_org();
        $this->add_closing_status();
        $this->setup_org_meta();

        /*
         * Fires at the end of closings init
         *
         * @param Closings $this The Closings instance
         */
        do_action('after_closings_init', $this);
        return true;
    }

    /**
     * admin_menu hook to add admin menus
     */
    public function add_admin_menus() {
        add_menu_page('Closings', 'Closings'
                      , 'close_school'
                      , 'closings'
                      , array($this, "output_closings_list")
                      , false
                      , 22
        );
        add_submenu_page(
            'closings'
            , __('Closings Administration', 'closings')
            , __('Closings Admin', 'closings')
            , 'admin_closings'
            , 'closings-admin'
            , array($this, "output_admin_page")
        );

        add_submenu_page(
            'closings'
            , $this->post_type->labels->add_new
            , $this->post_type->labels->add_new_item
            , $this->post_type->cap->edit_posts
            , "post-new.php?post_type=closing"
        );

        /*
         * To allow newsroom users to manage organizations
         *
         * If they shouldn't be able to, add the following code to your functions.php (or another plugin):
         *    add_filter('closings_newsroom_can_add', '__return_false');
         *
         * @param bool
         */
        $newsroom_can_add = apply_filters('closings_newsroom_can_add', true);
        if ($newsroom_can_add && $this->user_is_newsroom(wp_get_current_user())) {
            add_submenu_page(
                'closings',
                $this->taxonomy->labels->add_new,
                $this->taxonomy->labels->add_new_item,
                'close_all_schools',
                'add_org',
                array($this, "output_admin_page")
            );
            add_submenu_page(
                'closings',
                $this->taxonomy->labels->all_items,
                $this->taxonomy->labels->name,
                'close_all_schools',
                'edit-tags.php?taxonomy=closing_org&post_type=closing'
            );
        }

        // This is a fake page for export
        $page = add_submenu_page('closings_nowhere', __('Export', 'closings'), __('Export', 'closings'),
            'admin_closings', 'closings_export', array($this, 'output_admin_page'));
        add_action('load-' . $page, array($this, 'output_export'));
    }

    /**
     * Add javascript for closing form<br>
     * Called by initialize
     *
     * @param string $type
     */
    public function add_form_script($type) {
        if ($type == "closing") wp_enqueue_script('closings_post_script');
    }

    /**
     * Add closing form<br>
     * Called by initialize
     *
     * @param WP_Post $post
     */
    public function add_form($post) {
        if ($post->post_type != "closing") return;

        $notes = $post->post_content;
        $statuses = $this->get_statuses();
        $status = !empty($post->ID) ? get_post_meta($post->ID, '_closing_status', true) : false;

        $day = "today";
        $start = !empty($post->ID) ? get_post_meta($post->ID, '_closing_start', true) : false;
        if (empty($start)) $start = date('Y-m-d\TH:i', strtotime("$day 12am"));
        $end = !empty($post->ID) ? get_post_meta($post->ID, '_closing_end', true) : false;
        if (empty($end)) $end = date('Y-m-d\TH:i', strtotime("$day 6pm"));

        $day = date('Y-m-d', strtotime($start));

        require_once('status.tpl.php');
    }

    /**
     * save_post_closing hook<br>
     * Added by initialize<br>
     * Should be removed and added back if other functions call wp_update_post
     *
     * @param array $data
     * @param array $postarr
     * @return array
     */
    public function save_closing($post_id) {
		// If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

        /*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['_closings_nonce'] ) )
			return $post_id;

		// Verify that the nonce is valid.
		if ( ! check_admin_referer('add_closing', '_closings_nonce') )
			return $post_id;

        // TODO: Add permissions checking, maybe

        $status = sanitize_text_field($_POST['closing_status']);
        update_post_meta($post_id, '_closing_status', $status);

        $start = sanitize_text_field($_POST['closing_start']);
        $end = sanitize_text_field($_POST['closing_end']);

        $day = sanitize_text_field($_POST['closing_day']);

        if (!empty($day)) {
            // These should eventually be customizable, as should the statuses themselves
            switch ($status) {
            case "status_05": // 1 Hour Delay
                $start = $day . "T00:00";
                $end = $day . "T12:00";
                break;
            case "status_10": // 2 Hour Delay
                $start = $day . "T00:00";
                $end = $day . "T12:00";
                break;
            case "status_15": // Evening Cancelled
                $start = $day . "T14:00";
                $end = date('Y-m-d\TH:i', strtotime($day . " + 1 day - 1 second"));
                break;
            case "status_00": // Closed
            default:
                $start = $day . "T00:00";
                $end = $day . "T22:00";

            }
        }
        
        /*
         * Filter start and end time of closing
         *
         * Should return array in form of array($start, $end)<br>
         * Function may accept the following:<br>
         * $status: current closing status (in the form of an array key, such as status_00)<br>
         * $day: the date of the closing, in the format YYYY-MM-DD
         *
         * @param array $startend the current values
         * @param string $status the closing status
         * @param string $day the date of the closing
         */
        list($start, $end) = apply_filters('filter_closing_start_end', array($start, $end), $status, $day);

        update_post_meta($post_id, '_closing_start', $start);
        update_post_meta($post_id, '_closing_end', $end);

        $dayofweek = __(date('l', strtotime($start)));

        $user = wp_get_current_user();
        $current_user_org = $this->get_user_org($user);

        $orgs = wp_get_post_terms($post_id, 'closing_org');
        if (count($orgs) > 0) {
            $org = $orgs[0];

            if (count($orgs) > 1) {
                wp_set_object_terms($post_id, $org->term_id, 'closing_org', false);
            }
        }
        else $org = false;
        if ($this->user_is_user($user) && !empty($current_user_org)) {
            $proper_org = get_term_by('term_taxonomy_id', $current_user_org, 'closing_org');
            if (empty($org) || ($org->ID != $proper_org->ID)) {
                $org = $proper_org;
                wp_set_object_terms($post_id, $org->term_id, 'closing_org', false);
            }
        }

        if (is_object($org)) $org = $org->name;
        if (empty($org)) $org = __("Unknown organization", 'closings');

        $status = get_post_meta($post_id, '_closing_status', true);
        $statuses = $this->get_statuses();
        $status = isset($statuses[$status]) ? $statuses[$status] : __("Closing",'closings');

        $post_title = "$org: $status $dayofweek";
        $notes = sanitize_text_field($_POST['closing_notes']);

        // To avoid recursion
        remove_action('save_post_closing', array($this, "save_closing"), 1);

        // Update the content with "notes"
        wp_update_post(array('ID'=>$post_id, 'post_content' => $notes, 'post_title' => $post_title));

        // Add the filter back
        add_action('save_post_closing', array($this, "save_closing"), 1);
    }

    /**
     * admin_init hook<br>
     * queues admin styles
     */
    public function admin_init() {
        wp_enqueue_style('closing_admin_style');

        // add user css to hide extra stuff
        if ($this->user_is_user(wp_get_current_user())) {
            wp_enqueue_style('closing_admin_user_style');
        }
    }

    /**
     * wp_insert_post_data filter<br>
     * Added by initialize
     *
     * @param array $data
     * @param array $postarr
     * @return array
     */
    public function closing_save_filter($data, $postarr) {
		// If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $data;

        if ($data['post_type'] != "closing") return $data;

        $postid = $postarr['ID'];

        $orgs = isset($postarr['tax_input']['closing_org']) ? $postarr['tax_input']['closing_org'] : false;
        if (empty($orgs)) $orgs = wp_get_object_terms($postid, 'closing_org');

        if ((strpos($data['post_name'], "unknown-organization-closing") === 0)
            || (strpos($data['post_name'], "auto-draft") === 0)
            || (strpos($data['post_name'], "closing-for-unknown-organization") === 0)
        ) {
            $data['post_name'] = wp_unique_post_slug(
                "closing-" . uniqid(),
                $postid,
                $postarr['post_status'],
                $postarr['post_type'],
                $postarr['post_parent']
            );
        }

        return $data;
    }

    /**
     * user_has_cap filter<br>
     * Added by initialize
     *
     * @param array $allcaps
     * @param array $caps
     * @param array $args
     * @param WP_User $user
     * @return array
     */
    public function has_cap($allcaps, $caps, $args, $user) {
        $post_id = isset($args[2]) ? $args[2] : 0;
		$obj = get_post_type_object(get_post_type($post_id));
		if (!$obj) return $allcaps;
        if ($obj == $this->post_type) {

            // The coauthors plugin is conflicting with this plugin
            global $coauthors_plus;
            remove_filter('user_has_cap', array($coauthors_plus, 'filter_user_has_cap'), 10);

        }

        return $allcaps;
    }

    /**
     * redirect_post_location filter<br>
     * To redirect to the closings page after a closing is saved
     *
     * @param string $location
     * @param int | string $post_id
     * @return string 
     */
    public function redirect_after_save($location, $post_id) {
        if (isset($_POST['save']) || isset($_POST['publish'])) {
            if (preg_match("/post=([0-9]*)/", $location, $match)) {
                $post = get_post($post_id);
                if (empty($post)) return $location;
                if ($post->post_type == $this->post_type->name)
                    return admin_url('admin.php?page=closings&post=' . $post->ID);
            }
        }

        return $location;
    }

    /**
     * trashed_post hook<br>
     * To redirect to closings list after a closing is trashed
     *
     * @param int | string $postid
     */
    public function redirect_after_trash($postid) {
        $p = get_post($postid);
        $me = wp_get_current_user();
        $redirect_for_user = $this->user_is_user($me) || $this->user_is_newsroom($me);
        if ($redirect_for_user && $p->post_type == "closing") { 
            $message = sprintf(__("%s moved to the Trash"), $p->post_title);
            $url = add_query_arg(array('notices[]' => urlencode($message)), admin_url('admin.php?page=closings'));
            wp_redirect($url);
            exit;
        }
    }

    /**
     * closing_org_edit_form_fields hook<br>
     * Added by init
     *
     * @param stdClass $tag
     */
    public function org_link_to_user($tag) {
        $org = $this->get_orgs("include=" . $tag->term_id);
        if (!empty($org)) {
            $org = $org[0];
            if (!empty($org->users)) {
                echo '<tr class="form-field">';
                echo '<th scope="row">'.__('Closings User','closings').'</th>';
                echo '<td>';
                foreach($org->users as $key => $u) {
                    if ($key > 0) echo ", ";
                    printf('<a href="%s">%s</a>', admin_url('user-edit.php?user_id=' . $u->ID), $u->user_nicename);
                }
                echo '</td></tr>';
            }
        }
    }

    /**
     * login_redirect filter<br>
     * Added by initialize
     *
     * @param string $redirect_to
     * @param array $request
     * @param WP_User $user
     * @return string the URL to redirect to
     */
    public function redirect_user_login($redirect_to, $request, $user) {
        if ($this->user_is_user($user)) {
            return admin_url("admin.php?page=closings");
        } else return $redirect_to;
    }

    /**
     * wp_before_admin_bar_render action<br>
     * Removes dashboard link and Wordpress menu for Closing User
     */
    public function remove_home_button() {
        global $wp_admin_bar;

        if ($this->user_is_user(get_current_user_id())) {
            // Remove Dashboard link on public-facing site and WordPress logo menu everywhere.
            if (!is_admin()) {
                // Hide Dashboard link on public-facing site.
                $wp_admin_bar->remove_menu('dashboard');
            }

            // Hide WordPress logo menu completely.
            $wp_admin_bar->remove_menu('wp-logo');
        }
    }

    /**
     * admin_head action<br>
     * Removes dashboard link for Closing User
     */
    public function remove_dashboard() {
        if ($this->user_is_user(get_current_user_id())) {
            // Hides Dashboard menu.
            remove_menu_page('index.php');

            // Hides separator under Dashboard menu.
            remove_menu_page('separator1');
        }
    }

    /**
     * show_user_profile hook<br>
     * Adds user fields
     *
     * @param WP_User $user
     */
    public function user_fields($user) {
        if (!empty($user)) {
            if (in_array('closings-user',$user->roles)) {
                require('closing_user_fields.tpl.php');
            } 
        }
    }

    /**
     * personal_options_update hook<br>
     * Updates user fields on profile save
     */
    public function user_fields_update($userid) {
        if (!current_user_can('edit_user', $userid)) return false;

        update_user_meta($userid, "phone", $_POST['phone']);
        update_user_meta($userid, "alt_name", $_POST['alt_name']);
        update_user_meta($userid, "alt_email", $_POST['alt_email']);
        update_user_meta($userid, "alt_phone", $_POST['alt_phone']);
    }

    /**
     * Helper function to expire closings
     *
     * @param int | string $postid
     * @return int the id of the post, or 0 if failed
     */
    private function expire_closing($postid) {
        // To avoid unnecessary calls
        remove_action('save_post_closing', array($this, "save_closing"), 1);

        // Update the closings with the new status
        $r = wp_update_post(array('ID' => $postid, 'post_status' => "expired"));

        // Add the filter back
        add_action('save_post_closing', array($this, "save_closing"), 1);

        return $r;
    }

    /**
     * Sets status of expired closings to "expired"<br>
     * Run by WP-Cron<br>
     * Returns expired closings (an array of WP_Post)
     *
     * @see Closings::expire_closing()
     * @return array
     */
    public function expire_closings() {
        date_default_timezone_set($this->timezone);
        $expired = array();
        $now = time();
        $closings = $this->get_closings();

        foreach($closings as $cl) {
            $end = strtotime($cl->end);
            if ($end < $now) {
                // change status
                $cl->post_status = "expired";

                // Expire closing
                $this->expire_closing($cl->ID);

                // Add to array of expired closings
                $expired[] = $cl;
            }
        }

        date_default_timezone_set($this->default_timezone);
        return $expired;
    }

    /**
     * wp_ajax_import_closings hook<br>
     * Creates hook for ajax import
     */
    public function ajax_import() {
        $filename = $_POST['filename'];
        $new_orgs = $this->import($filename);
        echo json_encode(iterator_to_array($new_orgs));
        
        $new_orgs = null;
        unset($new_orgs);
        unlink($filename);
        wp_die();
    }

    /** @} */ // end of wphooks

    /** @defgroup admin Admin Config pages
     *  @{ 
     */

    /**
     * List closings (main screen in Closings admin)<br>
     * called by add_menu_page in add_admin_menus
     */
    public function output_closings_list() {
        $args = array(
            'post_type' => 'closing', 
            'post_status' => 'publish'
        );
        $closeall = true;
        if (!current_user_can("close_all_schools")) {
            $closeall = false;
            $org = get_user_meta(get_current_user_id(), 'closing_org', true);
            $args['tax_query'] = array(array(
                'taxonomy' => 'closing_org',
                'field' => 'term_taxonomy_id',
                'terms' => $org
            ));
        }

        $closings = $this->get_closings($args);
        require_once('closinglist.tpl.php');
    }

    /**
     * Admin page for closings<br>
     * called by add_submenu_page in add_admin_menus
     */
    public function output_admin_page() {
        $pages_out = $this->admin_page_links();
        if (empty($_GET['show'])) $_GET['show'] = "add_user";

        switch ($_GET['show']) {
        case "add_user":
            // each notice should be array('class'=>'space separated classes','text'=>'notice text')
            // Each notice also has classes of 'notice is-dismissible below-h2'
            $notices = array();
            $categories = $this->get_org_categories();
            
            if (isset($_POST['action'])) {
                switch($_POST['action']) {
                case "addorg":
                    // Verify that the nonce is valid.
                    if (!check_admin_referer('add_closings_org', '_wpnonce_add_closing_org'))
                        break 2;

                    $new = array();
                    if (!empty($_POST['user_login'])) {
                        $new_user_login = $new['user_login'] = wp_slash($_POST['user_login']);
                        $new_user_email = $new['email'] = sanitize_text_field(wp_unslash($_POST['email']));
                        $new['pass1'] = $_POST['pass1'];
                        $new['pass2'] = $_POST['pass2'];
                        if (!empty($_POST['first_name']))
                            $new_user_firstname = $new['first_name'] = sanitize_text_field($_POST['first_name']);
                        if (!empty($_POST['last_name']))
                            $new_user_lastname = $new['last_name'] = sanitize_text_field($_POST['last_name']);
                        $phone = $new['phone'] = sanitize_text_field($_POST['phone']);
                        $alt_name = $new['alt_name'] = sanitize_text_field($_POST['alt_name']);
                        $alt_email = $new['alt_email'] = sanitize_text_field($_POST['alt_email']);
                        $alt_phone = $new['alt_phone'] = sanitize_text_field($_POST['alt_phone']);
                    }
                    if (!empty($_POST['org_name'])) {
                        $org_name = $new['org_name'] = sanitize_text_field($_POST['org_name']);
                        $new['category'] = $_POST['org_cat'];
                        $org_street_address = $new['street_address'] = sanitize_text_field($_POST['org_street_address']);
                        $city = $new['city'] = sanitize_text_field($_POST['city']);
                        $state = $new['state'] = sanitize_text_field($_POST['state']);
                        $zip = $new['zip'] = sanitize_text_field($_POST['zip']);
                        $website = $new['website'] = sanitize_text_field($_POST['website']);
                        $notes = $new['notes'] = sanitize_text_field($_POST['notes']);
                    }

                    /*
                     * Filter new closings organization before saving
                     *
                     * @param array $new the new organization
                     * @param array $post the current $_POST values
                     */
                    $new = apply_filters('closings_new_org', $new, $_POST);
                    $new_org = $this->add_new_org($new);
                    
                    if (!is_wp_error($new_org)) {
                        $org_id = $new_org['org']->term_id;
                        $user_id = $new_org['user']->ID;
                        
                        $org_url = empty($org_id) ? false : admin_url("edit-tags.php?action=edit&taxonomy=closing_org&post_type=closing&tag_ID=" . $org_id);
                        $user_url = empty($user_id) ? false : admin_url("user-edit.php?user_id=" . $user_id);

                        $new_user_login = $new_user_email = $new_user_firstname = $new_user_lastname = false;
                        $phone = $alt_name = $alt_email = $alt_phone = false;
                        $org_name = $org_address = $website = $notes = false;

                        if (empty($org_url) && empty($user_url)) {
                            $errors->add('failed_add', __("Failed to add user and organization"));
                        } else {
                            $text = "New ";
                            if (!empty($org_url)) $text .= "<a href=\"{$org_url}\">organization</a> ";
                            if (!empty($org_url) && !empty($user_url)) $text .= "and ";
                            if (!empty($user_url)) $text .= "<a href=\"{$user_url}\">user</a> ";
                            $text .= "added.";
                            $notices[] = array(
                                'class' => 'updated notice-success',
                                'text' => $text
                            );
                        }
                    }

                    if (is_wp_error($new_org)) {
                        foreach($new_org->get_error_codes() as $code) {
                            $notices[] = array(
                                'class' => 'error ' . esc_attr($code). " error-". esc_attr($code) . " notice-failure",
                                'text' => $new_org->get_error_message($code)
                            );
                        }
                    }
                    break;
                }
            }
            
            require('adminsettings.tpl.php');
            break;
        case "expire":
            if (!$this->user_is_admin(wp_get_current_user())) goto def_page;
            $nonce_action = 'expire-closing';
            $nonce_name = "_wpnonce";
            if (!check_admin_referer($nonce_action, $nonce_name)) break;
            $postid = $_GET['post'];
            $post = get_post($postid);
            if ($post->post_status == "publish") {
                $r = $this->expire_closing($postid);
                if ($r > 0) $message = sprintf(__("%s has been expired"), $post->post_title);
                else $message = sprintf(__("%s could not be expired"), $post->post_title);
            } else $message = sprintf(__("%s was not publised"), $post->post_title);
            $red_url = add_query_arg(array('notices[]' => urlencode($message)),admin_url('admin.php?page=closings'));
            // We need to redirect with javascript, as output has already been sent
            printf('<script type="text/javascript">window.location.replace(%s);</script>', json_encode($red_url));

            break;
        case "mod_users":
            if (!$this->user_is_admin(wp_get_current_user())) goto def_page;
            $users = $this->get_closing_users();
            $orgs = $this->get_orgs();
            $adduserurl = add_query_arg(array('show' => 'add', 'type' => 'user'));
            $addnewsroomurl = add_query_arg(array('show' => 'add', 'type' => 'newsroom'));
            $addadminurl = add_query_arg(array('show' => 'add', 'type' => 'admin'));
            require('modusers.tpl.php');
            break;
        case "add":
        case "remove":
            if (!$this->user_is_admin(wp_get_current_user())) goto def_page;
            $valid_types = array('admin', 'newsroom', 'user');
            $action = $_GET['show'];
            $adduser = false;

            $user_id = intval($_GET['user']) < 1 ? false : intval($_GET['user']);
            $type = in_array($_GET['type'], $valid_types, true) ? $_GET['type'] : false;
            if (($type == "user") && ($action == "add")) {
                $org_id = intval($_GET['org']) < 1 ? false : intval($_GET['org']);
                $adduser = true;
            }

            $nonce_action = ($action == "add" ? "add" : "del") . "_org_" . $type;
            $nonce_name = "_wpnonce_" . $nonce_action;
            if (!check_admin_referer($nonce_action, $nonce_name)) break;

            $method = $type . "_privileges";
            $callable = array($this, $method);
            call_user_func($callable, $user_id, $action == "remove");
            if ($adduser) update_user_meta($user_id, 'closing_org', $org_id);

            $notices = array();

            $user = new WP_User($user_id);
            $method = "user_is_" . $type;
            $callable = array($this, $method);
            $success = call_user_func($callable, $user);
            if ($action == "remove") {
                // If we removed the privileges, $success should be false
                $notices[] = array(
                    'text' => sprintf(__(($success ? "Uns" : "S") . "uccessfully removed %s privileges from %s", 'closings'), $type, $user->display_name),
                    'class' => $success ? "updated notice-success" : "notice-failure"
                );
            } else {
                $notices[] = array(
                    'text' => sprintf(__("%s " . ($success ? "" : "un") . "successfully given %s privileges", 'closings'), $user->display_name, $type),
                    'class' => $success ? "updated notice-success" : "notice-failure"
                );

                if ($adduser) {
                    $org = get_term_by('term_taxonomy_id', $org_id, 'closing_org');
                    if ($this->get_user_org($user_id) == $org_id) {
                        $notices[] = array(
                            'text' => sprintf(__("%s can close %s", 'closings'), $user->display_name, $org->name),
                            'class' => "updated notice-success"
                        );
                    } else {
                        $notices[] = array(
                            'text' => sprintf(__("Failed to make %s closer for %s", 'closings'), $user->display_name, $org->name),
                            'class' => "notice-failure"
                        );
                    }
                }
            }

            $users = $this->get_closing_users();
            $orgs = get_terms('closing_org', array('hide_empty'=>false));
            require('modusers.tpl.php');
            break;
        case "import":
            if (!$this->user_is_admin(wp_get_current_user())) goto def_page;
            $upload = false;
            if (isset($_GET['_wpnonce'])) {
                // We might have an upload

                // Check the nonce
                check_admin_referer('import-upload');

                $upload = true;
                $ajax = false;

                $file = $_FILES['import']['tmp_name'];
                $filename = $_FILES['import']['name'];

                // Had problems with large files, so we split them up and process separately with an AJAX call
                $size = @filesize($file);
                if ($size > 19000) {
                    /*
                      This number was somewhat random. Basically, I had a very large file of ~1000 lines, and figured 
                      that 50 lines per file should work, so 19000 is 1/20 of that file's size.
                    */
                    $ajax = true;
                    $valid = $this->valid_csv($file);

                    if (!$valid) {
                        $ajax = false;
                        $new_orgs = $this->import($file);
                    } else {
                        $length = 0; // # of lines in file
                        $total = 0;  // # of files
                        $filebase = tempnam(sys_get_temp_dir(), "closings_");

                        // File to read in
                        $in = fopen($file, "r");
                        $head = fgets($in);

                        // Start off with the first file
                        $newfile = $filebase . (++$total);
                        $out = fopen($newfile, "w");
                        fwrite($out, $head);

                        while ($line = fgets($in)) {
                            if ($length > 48) {
                                // File is long enough. Start a new one
                                fclose($out);
                                $length = 0;
                                $newfile = $filebase . (++$total);
                                $out = fopen($newfile, "w");
                                fwrite($out, $head);
                            }
                            fwrite($out, $line);
                            $length++;
                        }
                        // Close the files
                        fclose($out);
                        fclose($in);
                    }
                } else $new_orgs = $this->import($file);
            }

            require('import.tpl.php');
            break;
        default:
        def_page:
            /*
             * Fires when a custom admin page is rendered
             *
             * @param array $pages_out The HTML rendering of the admin pages
             */
            do_action('closing_admin_page_' . $_GET['show'], $pages_out);
            if (!has_action('closing_admin_page_' . $_GET['show'])) {
                /*
                 * Fires when a default custom admin page is rendered<br>
                 * Only fires if no closing_admin_page_xxx hook fired
                 *
                 * @param array $pages_out The HTML rendering of the admin pages
                 */
                do_action('closing_admin_page_default', $_GET['show'], $pages_out);
            }
        }
    }

    /**
     * hook for downloading export
     */
    public function output_export() {
        // set headers
        $file_name = 'closings-export-' . date('Y-m-d') . '.csv';

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename={$file_name}");
        header("Content-type: text/plain");

        $output = fopen("php://output", "w");
        $this->export($output);
        fclose($output);
        die;
    }

    /** @} */ // end of admin group

    /** @defgroup init Plugin initialization functions 
     *  @{
     */

    /**
     * Add closing post type<br>
     * Called by initialize
     */
    public function add_closing_post_type() {
        $labels = array(
            'name'                => _x( 'Closings', 'Post Type General Name', 'closings' ),
            'singular_name'       => _x( 'Closing', 'Post Type Singular Name', 'closings' ),
            'menu_name'           => __( 'Closings', 'closings' ),
            'name_admin_bar'      => __( 'Closing', 'closings' ),
            'parent_item_colon'   => __( 'Parent Item:', 'closings' ),
            'all_items'           => __( 'All Closings', 'closings' ),
            'add_new_item'        => __( 'Add Closing', 'closings' ),
            'add_new'             => __( 'Add Closing', 'closings' ),
            'new_item'            => __( 'New Closing', 'closings' ),
            'edit_item'           => __( 'Edit Closing', 'closings' ),
            'update_item'         => __( 'Update Closing', 'closings' ),
            'view_item'           => __( 'View Closing', 'closings' ),
            'search_items'        => __( 'Search Closing', 'closings' ),
            'not_found'           => __( 'Not found', 'closings' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'closings' ),
        );
        $capabilities = array(
            'edit_post'           => 'close_school',
            'read_post'           => 'read_post',
            'delete_post'         => 'close_school',
            'edit_posts'          => 'close_school',
            'edit_others_posts'   => 'close_all_schools',
            'publish_posts'       => 'close_school',
            'create_posts'        => 'close_school',
            'read_private_posts'  => 'read_private_posts',
        );
        $args = array(
            'label'               => __( 'closing', 'closings' ),
            'description'         => __( 'Closing Description', 'closings' ),
            'labels'              => $labels,
            'supports'            => array('author'),
            'taxonomies'          => array('closing_org'),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'menu_position'       => 25,
            'show_in_admin_bar'   => false,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,		
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capabilities'        => $capabilities,
        );
        register_post_type( 'closing', $args );

        $this->post_type = get_post_type_object('closing');
    }

    /**
     * Add closing_org taxonomy<br>
     * Called by initialize
     */
    public function add_closing_org() {
        $labels = array(
            'name'                       => _x( 'Closing Organizations', 'Taxonomy General Name', 'closings' ),
            'singular_name'              => _x( 'Closing Organization', 'Taxonomy Singular Name', 'closings' ),
            'menu_name'                  => __( 'Organizations', 'closings' ),
            'all_items'                  => __( 'All Organizations', 'closings' ),
            'parent_item'                => __( 'Parent Organization', 'closings' ),
            'parent_item_colon'          => __( 'Parent Organization:', 'closings' ),
            'new_item_name'              => __( 'New Organization', 'closings' ),
            'add_new_item'               => __( 'Add Organization', 'closings' ),
            'edit_item'                  => __( 'Edit Organization', 'closings' ),
            'update_item'                => __( 'Update Organization', 'closings' ),
            'view_item'                  => __( 'View Organization', 'closings' ),
            'separate_items_with_commas' => __( 'Separate organizations with commas', 'closings' ),
            'add_or_remove_items'        => __( 'Add or remove organizations', 'closings' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'closings' ),
            'popular_items'              => __( 'Popular Organizations', 'closings' ),
            'search_items'               => __( 'Search Organizations', 'closings' ),
            'not_found'                  => __( 'Not Found', 'closings' ),
        );

        // This filter documented in add_admin_menus
        $manage_terms = apply_filters('closings_newsroom_can_add', true) ? "close_all_schools" : "admin_closings";
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => false,
            'public'                     => true,
            'show_ui'                    => true,
            'show_in_menu'               => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => false,
            'capabilities'               => array(
                'manage_terms' => $manage_terms,
                'edit_terms'   => $manage_terms,
                'delete_terms' => $manage_terms,
                'assign_terms' => 'close_all_schools'
            )
        );
        register_taxonomy( 'closing_org', array( 'closing' ), $args );
        $this->taxonomy = get_taxonomy('closing_org');
    }

    /**
     * Add expired status for closings<br>
     * Called by initialize
     */
    public function add_closing_status() {
        register_post_status(
            'expired', array(
                'label' => __('Expired', 'closings'),
                'label_count' => _n_noop( 'Expired (%s)',  'Expired (%s)', 'closings' ), 
                'public' => false,
                'exclude_from_search' => true,
                
            ));
    }

    /**
     * Add meta fields to closing org<br>
     * Called by initialize
     */
    private function setup_org_meta() {
        // This is better documented in https://github.com/bainternet/Tax-Meta-Class/blob/master/class-usage-demo.php
        
        $config = array(
            'id' => 'closing_org_meta_box',   // meta box id, unique per meta box
            'title' => 'Closing Org Meta Box',// meta box title
            'pages' => array('closing_org'),  // taxonomy name, accept categories, post_tag and custom taxonomies
            'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
            'fields' => array(),              // list of meta fields (can be added by field arrays)
            'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
            'use_with_theme' => false         //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
        );
  
  
        $my_meta =  new Tax_Meta_Class($config);

        // Category
        $categories = $this->get_org_categories();
        $my_meta->addSelect($this->prefix.'category', $categories, array('name'=> __('Category','closings')));

        $my_meta->addText($this->prefix.'street_address',array('name'=> __('Street Address')));
        $my_meta->addText($this->prefix.'city',array('name'=> __('City')));
        $my_meta->addText($this->prefix.'state',array('name'=> __('State/Province')));
        $my_meta->addText($this->prefix.'zip',array('name'=> __('Zip/Postal Code')));
        $my_meta->addText($this->prefix.'website',array('name'=> __('Website')));
        $my_meta->addTextarea($this->prefix.'notes',array('name'=> __('Notes')));

        /*
         * Modify organization meta<br>
         * See https://github.com/bainternet/Tax-Meta-Class for into on Tax_Meta_Class
         * 
         * @param Tax_Meta_Class $meta
         * @param string $prefix string to prepend to tax names
         */
        $my_meta = apply_filters('closings_org_meta', $my_meta, $this->prefix);

        //Finish Meta Box Decleration
        $my_meta->Finish();
    }

    /**
     * Provides the `[closings]` shortcode
     *
     * @see Closings::output_closings()
     * @return string
     */
    public function closings_shortcode($atts) {
        return $this->output_closings($atts, false);
    }

    /**
     * Provides the `[closings_by_state]` shortcode
     *
     * @see Closings::output_closings_by_state()
     * @return string
     */
    public function closings_by_state_shortcode($atts) {
        return $this->output_closings_by_state($atts, false);
    }

    /** @} */

    /** @defgroup template Template functions
     *  @{
     */

    /**
     * Outputs a table of closings
     *
     * @param array $args optional parameters to pass to get_closings
     * @param bool $echo optional set to false to return, instead of output
     * @return bool | string
     */
    public function output_closings($args = null, $echo = true) {
        $closings = $this->get_closings($args);
        if (!$echo) ob_start();
        include('closings_table.tpl.php');
        if (!$echo) {
            $r = ob_get_clean();
            return $r;
        }
        return true;
    }

    /**
     * Outputs a table of closings, sorted by state and organization type
     *
     * @param array $args optional parameters to pass to get_closings_by_state
     * @param bool $echo optional set to false to return, instead of output
     * @return bool | string
     */
    public function output_closings_by_state($args = null, $echo = true) {
        $closings = $this->get_closings_by_state(null, $args);
        if (!$echo) ob_start();
        include('closings_by_state_table.tpl.php');
        if (!$echo) {
            $r = ob_get_clean();
            return $r;
        }
        return true;
    }

    /**
     * Returns the admin page links.
     *
     * @param bool $html optional set to false to return an array of links, rather than the generated HTML
     * @return string | array
     */
    public function admin_page_links($html = true) {
        $pages = array(
            array('url' => add_query_arg(array('page' => 'closings-admin', 'show' => 'add_user'), admin_url('admin.php'))
                  , 'label' => __('Add New Organization')),
            array('url' => add_query_arg(array('taxonomy' => 'closing_org', 'post_type' => 'closing'), admin_url('edit-tags.php'))
                  , 'label' => __('Organizations Listing', 'closings')),
            array('url' => add_query_arg(array('page' => 'closings-admin', 'show' => 'mod_users'), admin_url('admin.php'))
                  , 'label' => __('Modify Users', 'closings')),
            array('url' => add_query_arg(array('post_type' => 'closing'), admin_url('edit.php'))
                  , 'label' => $this->post_type->labels->all_items),
            array('url' => add_query_arg(array('page' => 'closings-admin', 'show' => 'import'), admin_url('admin.php'))
                  , 'label' => __('Import', 'closings')),
        );

        /*
         * Filter admin header menu
         * 
         * @param array $states
         */
        $pages = apply_filters('closing_admin_pages_header', $pages);
        if (!$this->user_is_admin(wp_get_current_user())) $pages = array();
        if (!$html) return $pages;

        $pages_out = "<ul class=\"subsubsub\">";
        foreach($pages as $k => $p) $pages_out .= "<li><a href=\"{$p['url']}\">{$p['label']}</a>".($k<count($pages)-1?" | ":"")."</li>\n";
        $pages_out .= "</ul><div class=\"clear\"></div>";

        return $pages_out;
    }

    /** @} */

    /** @defgroup util Utility functions
     *  @{
     */

    /**
     * adds a new org and user
     *
     * Takes a single associative array as a parameter
     *
     * arguments for user are: user_login, pass1, pass2, first_name, last_name, email, phone, alt_name, alt_email, alt_phone<br>
     * arguments for org are: org_name, category, street_address, city, state, zip, url
     *
     * category should be one of the keys returned by $this->get_org_categories()
     *
     * To add user without org, don't set user_login<br>
     * To add org without user, don't set org_name
     *
     * On success, returns an array like array('org'=>{org_id},'user'=>{user_id})<br>
     * org is the id of the organization created, or false<br>
     * user is the id of the user created, or false
     *
     * On failure, returns a WP_Error
     *
     * @param array | string $args
     * @return array | WP_Error
     */
    public function add_new_org($args) {
        // TODO: Check permissions, maybe
        
        $defaults = array(
            'user_login' => null,
            'pass1' => null,
            'pass2' => null,
            'first_name' => null,
            'last_name' => null,
            'email' => null,
            'phone' => null,
            'alt_name' => null,
            'alt_email' => null,
            'alt_phone' => null,
            
            'org_name' => null,
            'category' => null,
            'street_address' => null,
            'city' => null,
            'state' => null,
            'zip' => null,
            'website' => null,
            'notes' => null,
        );

        $args = wp_parse_args($args, $defaults);
        $categories = $this->get_org_categories();

        $org_id = $user_id = false;
        $errors = new WP_Error();
        $user_errors = new WP_Error();
        $org_errors = new WP_Error();
        $user = $org = $added = false;
        if (!empty($args['user_login'])) {
            // Copied much of this code from wp-admin/includes/user.php
            $user = new stdClass;
                        
            $pass1 = $pass2 = '';
            if (isset($args['pass1'])) $pass1 = $args['pass1'];
            if (isset($args['pass2'])) $pass2 = $args['pass2'];
                        
            $user->user_login = $args['user_login'];
            $user->user_email = $args['email'];
                        
            if ( $user->user_login == '' ) $user_errors->add( 'user_login', __( '<strong>ERROR</strong>: Please enter a username.' ) );
            else $new_user_login = wp_unslash($user->user_login);
                        
            do_action_ref_array( 'check_passwords', array( $user->user_login, &$pass1, &$pass2 ) );
            if ( empty($pass1) )
                $user_errors->add( 'pass', __('<strong>ERROR</strong>: Please enter your password.'), array('form-field' => 'pass1'));
            elseif ( empty($pass2) )
                $user_errors->add( 'pass', __('<strong>ERROR</strong>: Please enter your password twice.'), array('form-field' => 'pass2'));

            /* Check for "\" in password */
            if ( false !== strpos(wp_unslash( $pass1 ), "\\"))
                $user_errors->add('pass', __('<strong>ERROR</strong>: Passwords may not contain the character "\\".'), array('form-field' => 'pass1'));

            /* checking the password has been typed twice the same */
            if ( $pass1 != $pass2 )
                $user_errors->add('pass', __('<strong>ERROR</strong>: Please enter the same password in the two password fields.'), array('form-field' => 'pass1'));

            if (!empty($pass1)) $user->user_pass = $pass1;

            if (!validate_username($args['user_login']))
                $user_errors->add('user_login', __('<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.'));
            if (username_exists($user->user_login))
                $user_errors->add('user_login', __('<strong>ERROR</strong>: This username is already registered. Please choose another one.'));

            if ( empty( $user->user_email ) ) {
                $user_errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please enter an e-mail address.' ), array( 'form-field' => 'email' ) );
            } elseif ( !is_email( $user->user_email ) ) {
                $user_errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ), array( 'form-field' => 'email' ) );
            } elseif (email_exists($user->user_email)) {
                $user_errors->add('email_exists', __('<strong>ERROR</strong>: This email is already registered, please choose another one.'), array('form-field' => 'email'));
            } else $new_user_email = $user->user_email;

            if (isset($args['first_name'])) 
                $new_user_firstname = $user->first_name = $args['first_name'];
            if (isset($args['last_name'])) 
                $new_user_lastname = $user->last_name = $args['last_name'];

            do_action_ref_array( 'user_profile_update_errors', array( &$user_errors, false, &$user ) );
        }
        if (!empty($args['org_name'])) {
            $org = new stdClass;
            $org->name = $args['org_name'];
            if ($org->name == '') $org_errors->add('invalid_org_name',__('Invalid organization name chosen', 'closings'));
                        
            $org->category = $args['category'];
            if (!isset($categories[$org->category])) {
                $org->category = '';
                $org_errors->add('invalid_org_cat',__('Invalid category chosen', 'closings'));
            }
                        
            $org->street_address = $args['street_address'];
            $org->city = $args['city'];
            $org->state = $args['state'];
            $org->zip = $args['zip'];
            $org->website = $args['website'];
            $org->notes = $args['notes'];
        }

        foreach($user_errors->get_error_codes() as $code) $errors->add($code, $user_errors->get_error_message($code), $user_errors->get_error_data($code));
        foreach($org_errors->get_error_codes() as $code) $errors->add($code, $org_errors->get_error_message($code), $org_errors->get_error_data($code));

        if (!$errors->get_error_codes()) {
            if ($user) {
                $user_id = wp_insert_user($user);
                if (is_wp_error($user_id)) {
                    $user_id = false;
                } else {
                    // Set user's role to closings-user
                    $user = new WP_User($user_id);
                    $user->set_role('closings-user');
                    $this->user_privileges($user);
                    if (!empty($args['phone'])) {
                        update_user_meta($user_id, 'phone', $args['phone']);
                        $user->phone = get_user_meta($user_id, 'phone', true);
                    }
                    if (!empty($args['alt_name'])) {
                        update_user_meta($user_id, 'alt_name', $args['alt_name']);
                        $user->alt_name = get_user_meta($user_id, 'alt_name', true);
                    }
                    if (!empty($args['alt_email'])) {
                        update_user_meta($user_id, 'alt_email', $args['alt_email']);
                        $user->alt_email = get_user_meta($user_id, 'alt_email', true);
                    }
                    if (!empty($args['alt_phone'])) {
                        update_user_meta($user_id, 'alt_phone', $args['alt_phone']);
                        $user->alt_phone = get_user_meta($user_id, 'alt_phone', true);
                    }
                    $user->password = $args['pass1'];
                    // Further down, do update_user_meta($user_id, 'closing_org', $org_term_id)

                    /*
                     * Fires after a new closings user is added
                     *
                     * @param WP_User $user the new organization
                     * @param array $args the original arguments passed to the function
                     */
                    do_action('closings_new_org_after_user_insert', $user, $args, $this->prefix);
                }
            }
            $user_added = (!!$user) ? (!!$user_id) : true;
            if ($org) {
                if (empty($org->name) || empty($org->category)) $org_id = false;
                else {
                    $org_r = wp_insert_term($org->name, "closing_org");
                    if (is_wp_error($org_r)) $org_id = false;
                    else {
                        if (!empty($user_id)) update_user_meta($user_id, 'closing_org', $org_r['term_taxonomy_id']);
                                    
                        $org->id = $org_id = $org_r['term_id'];
                        update_tax_meta($org_id, $this->prefix.'category', $org->category);
                        if (!empty($org->street_address)) update_tax_meta($org_id, $this->prefix.'street_address', $org->street_address);
                        if (!empty($org->city   )) update_tax_meta($org_id, $this->prefix.'city', $org->city);
                        if (!empty($org->state  )) update_tax_meta($org_id, $this->prefix.'state', $org->state);
                        if (!empty($org->zip    )) update_tax_meta($org_id, $this->prefix.'zip', $org->zip);
                        if (!empty($org->address)) update_tax_meta($org_id, $this->prefix.'address', $org->address);
                        if (!empty($org->website)) update_tax_meta($org_id, $this->prefix.'website', $org->website);
                        if (!empty($org->notes  )) update_tax_meta($org_id, $this->prefix.'notes', $org->notes);
                        
                        /*
                         * Fires after a new closings organization is added
                         *
                         * @param stdClass $org the new organization
                         * @param array $args the original arguments passed to the function
                         */
                        do_action('closings_new_org_after_term_insert', $org, $args, $this->prefix);
                        $org = $this->get_orgs('include='.$org_id)[0];
                    }
                }
            }
            $org_added = (!!$org) ? (!!$org_id) : true;
            if ($user_added && !$org_added) {
                // Added the user, but not the org
                // Gotta clear out the user values, and add an error 
                $message = "User added, but not organization. Add organization and then <a href=\"%s\">Modify Users</a>";
                $message = sprintf(__($message, 'closings'), esc_attr(admin_url('admin.php?page=closings-admin&show=mod_users')));
                $errors->add('org_failed_to_add', $message);

                $new_user_login = $new_user_email = $new_user_firstname = $new_user_lastname = false;
            }
        }

        if ($errors->get_error_codes()) $errors->add_data($args, $errors->get_error_codes()[0]);
        $r = (!$errors->get_error_codes()) ? array('user' => $user, 'org' => $org) : $errors;

        /*
         * Fires after a new organization is added<br>
         * Useful for performing post-add actions, such as emailing the new user
         *
         * @param array | WP_Error $r if add was successful, an array with the WP_User and taxonomy, else a WP_Error
         */
        do_action('closings_add_org', $r);

        return $r;
    }

    /**
     * Check validity of CSV import file
     *
     * @see Closings::import()
     * @param string $file path to file
     * @return bool
     */
    private function valid_csv($file) {
        if (!is_file($file)) return false;
        if (!is_readable($file)) return false;

        $handle = fopen($file, "r");
        $header = fgetcsv($handle);
        $have_headers = in_array("email", $header) && in_array("org_name", $header);
        fclose($handle);

        return $have_headers;
    }

    /**
     * import data from csv
     *
     * @see ClosingsImportIterator
     * @param string $file path to file
     * @return ClosingsImportIterator | WP_Error iterator to loop over data, or WP_Error on failure
     */
    public function import($file) {
        try {
            $iter = new ClosingsImportIterator($file);
        } catch (ClosingsIteratorException $e) {
            return new WP_Error($e->getCode(), $e->getMessage());
        }
        return $iter;
    }

    /**
     * export data to csv<br>
     * The calling method should close the handle when done
     *
     * @param handle $handle an open, writable file handle
     */
    public function export($handle) {
        $fields = apply_filters('closing_export_fields', array(
            'email', 'user_login', 'first_name', 'last_name', 'phone', 'alt_name', 'alt_email', 'alt_phone',
            'org_name', 'street_address', 'city', 'state', 'zip', 'category', 'website', 'notes'
        ));

        fputcsv($handle, $fields);
        foreach($this->get_orgs() as $org) {
            $out = array();
            foreach($fields as $f) {
                if ($f == "email") $f = "user_email";
                if ($f == "org_name") $f = "name";
                if (isset($org->users[0])) {
                    if (isset($org->users[0]->{$f})) {
                        $out[] = $org->users[0]->{$f};
                        continue;
                    }
                }
                if (isset($org->{$f})) $out[] = $org->{$f};
                else $out[] = "";
            }
            $out = apply_filters('closing_export_organization', $out, $org);
            fputcsv($handle, $out);
        }
    }

    /**
     * gets users<br>
     * returns associative array of arrays:
     *
     *     array('admin'=>array(), 'newsroom'=>array(), 'users'=>array(), 'non_users'=>array())
     *
     * @return array
     */
    public function get_closing_users() {
        $all_users = get_users();
        $admins = array();
        $newsroom = array();
        $users = array();
        $non_users = array();

        foreach($all_users as $u) {
            if ($u->has_cap('admin_closings')) $admins[$u->user_login] = $u;
            elseif ($u->has_cap('close_all_schools')) $newsroom[$u->user_login] = $u;
            elseif ($u->has_cap('close_school')) {
                $org = get_user_meta($u->ID, 'closing_org', true);
                $u->org = get_term_by('term_taxonomy_id', $org, 'closing_org');
                $users[$u->user_login] = $u;
            }
            else $non_users[$u->user_login] = $u;
        }
        return array(
            'admins' => $admins,
            'newsroom' => $newsroom,
            'users' => $users,
            'non_users' => $non_users
        );
    }

    /**
     * Get the status of a particular closing
     *
     * @param int $postid
     * @param bool $code option set true to return the code, rather than the name
     * @return string
     */
    public function get_closing_status($postid, $code = false) {
        $status = get_post_meta($postid, "_closing_status", true);
        if ($code) return $status;
        
        $statuses = $this->get_statuses();
        return isset($statuses[$status]) ? $statuses[$status] : "Closing";
    }

    /**
     * Returns the organization for the user
     *
     * @param int | WP_User $user, the WP_User, or id of the user. Defaults to current user
     * @return int the term_taxonomy_id of the organization
     */
    public function get_user_org($user = null) {
        if (empty($user)) $user = get_current_user_id();
        if (!$this->user_is_user($user)) return false;

        if (is_int($user)) $user_id = $user;
        elseif ($user instanceof WP_User) $user_id = $user->ID;
        else $user_id = get_current_user_id();

        return get_user_meta($user_id, 'closing_org', true);
    }

    /**
     * Helper for get_org* functions
     *
     * @param object $org a term return by get_term*
     * @return object same term with additional information added
     */
    private function get_org_helper($org) {
        if (empty($org)) return false;
        if (!is_object($org)) return false;
        $categories = $this->get_org_categories();

        $org->category_code = get_tax_meta($org->term_id, $this->prefix.'category');
        $org->category = isset($categories[$org->category_code]) ? $categories[$org->category_code] : __("None", 'closings');
        $org->street_address = get_tax_meta($org->term_id, $this->prefix.'street_address');
        $org->city     = get_tax_meta($org->term_id, $this->prefix.'city');
        $org->state    = get_tax_meta($org->term_id, $this->prefix.'state');
        $org->zip      = get_tax_meta($org->term_id, $this->prefix.'zip');
        $org->website  = get_tax_meta($org->term_id, $this->prefix.'website');
        $org->notes    = wp_unslash(get_tax_meta($org->term_id, $this->prefix.'notes'));
        $org->users    = array_merge(get_users(array('meta_key'=>'closing_org','meta_value'=>$org->term_taxonomy_id,'fields'=>'all_with_meta')));

        /*
         * Filter org
         *
         * @param object $org
         */
        return apply_filters('get_closing_org', $org);
    }

    /**
     * Gets a single organization, by a given field
     *
     * @see Closings::get_org_helper()
     * @param string $field Either 'id', 'slug', 'name', or 'term_taxonomy_id'
     * @param string | int $value Search for this term value
     * @return object
     */
    public function get_org_by($field, $value) {
        return $this->get_org_helper(get_term_by($field, $value, 'closing_org'));
    }

    /**
     * Get organizations, by a given field
     *
     * @see Closings::get_org_helper()
     * @see Closings::get_org_by()
     * @param string $field Either 'id', 'slug', 'name', or 'term_taxonomy_id'
     * @param array $values Searches for this term value
     * @return array of objects
     */
    public function get_orgs_by($field, $values) {
        $orgs = array();
        foreach($values as $v) {
            if ($org = $this->get_org_by($field, $v))
                $orgs[] = $org;
        }

        /*
         * Filter orgs
         *
         * @param array $orgs
         */
        return apply_filters('closings_orgs', $orgs);
    }

    /**
     * Gets closing organizations
     *
     * @see Closings::get_org_helper()
     * @param array $args optional args to pass to get_terms
     * @return array of objects
     */
    public function get_orgs($args = null) {
        $defaults = array('hide_empty' => false);
        $params = wp_parse_args($args, $defaults);
        $orgs = get_terms('closing_org', $params);

        foreach ($orgs as &$org) {
            $org = $this->get_org_helper($org);
        }

        /*
         * Filter orgs
         * 
         * @param array $orgs
         */
        return apply_filters('closings_orgs', $orgs);
    }


    /**
     * Gets status of an organization
     *
     * @param stdClass | int | string $org may be an object return by get_orgs, the term_id, a slug, or parameters to pass to get_closings
     * @return WP_Post | stdClass an object representing current status
     */
    public function get_org_status($org) {
        date_default_timezone_set($this->timezone);
        if (!is_object($org)) {
            if (is_numeric($org)) $orgs = $this->get_orgs('include=' . $org);
            elseif (is_string($org) && !strpos($org, '=')) $orgs = $this->get_orgs('slug=' . $org);
            else $orgs = $this->get_orgs($org);

            if (count($orgs) > 0) $org = $orgs[0];
            else $org = new stdClass;
        }

        // If this is not a valid org, return false
        if (empty($org->slug)) return false;

        $closings = $this->get_closings("closing_org=" . $org->slug);
        if (count($closings) < 1) {
            $closing = new stdClass;
            $closing->ID = 0;
            $closing->post_author = '0';
            $closing->post_title = $org->name . ": " . __("Open", 'closings');
            $closing->post_status = 'publish';
            $closing->post_name = 'closing-open-' . uniqid();
            $closing->status = __("Open", 'closings');
            $closing->status_code = "status_open";
            $closing->note = "";
            $closing->post_date = date('Y-m-d H:i:s');
            $closing->start = date('Y-m-d\TH:i');
            $closing->end = date('Y-m-d\TH:i');
            $closing->dayofweek = __(date('l'));
            $closing->org = $org;
        } else $closing = $closings[0];

        date_default_timezone_set($this->default_timezone);
        return $closing;
    }

    /**
     * Gets current status of an organization
     *
     * @param stdClass | int | string $org may be an object return by get_orgs, the term_id, a slug, or parameters to pass to get_closings
     * @param int | string $ts timestamp, or time name (such as "tomorrow"), optional time to check, defaults to "now"
     * @return WP_Post | stdClass an object representing current status
     */
    public function get_current_org_status($org, $ts = "now") {
        date_default_timezone_set($this->timezone);
        if (is_string($ts)) $ts = strtotime($ts);
        if (!is_object($org)) {
            if (is_numeric($org)) $orgs = $this->get_orgs('include=' . $org);
            elseif (is_string($org) && !strpos($org, '=')) $orgs = $this->get_orgs('slug=' . $org);
            else $orgs = $this->get_orgs($org);

            if (count($orgs) > 0) $org = $orgs[0];
            else $org = new stdClass;
        }

        // If this is not a valid org, return false
        if (empty($org->slug)) return false;

        $closing = new stdClass;
        $closing->ID = 0;
        $closing->post_author = '0';
        $closing->dayofweek = __(date('l', $ts));
        $closing->post_title = sprintf('%s: %s %s', $org->name, __("Open", 'closings'), $closing->dayofweek);
        $closing->post_status = 'publish';
        $closing->post_name = 'closing-open-' . uniqid();
        $closing->status = __("Open", 'closings');
        $closing->status_code = "status_open";
        $closing->note = "";
        $closing->post_date = date('Y-m-d H:i:s');
        $closing->start = date('Y-m-d\TH:i', $ts);
        $closing->end = date('Y-m-d\TH:i', $ts);
        $closing->org = $org;

        $closings = $this->get_closings("closing_org=" . $org->slug);

        if (count($closings) > 0) {
            foreach($closings as $cl) {
                $start = strtotime($cl->start);
                $end = strtotime($cl->end);
                if ($start <= $ts && $ts <= $end) {
                    $closing = $cl;
                    break;
                }
            }
        }

        date_default_timezone_set($this->default_timezone);
        return $closing;
    }

    /**
     * Gets current closings
     *
     * @param array $args optional args to pass to get_posts
     * @return array of WP_Post objects
     */
    public function get_closings($args = null) {
        $defaults = array(
            'post_type' => 'closing',
            'posts_per_page' => -1
        );
        $params = wp_parse_args($args, $defaults);
        $closings = get_posts($params);
        $categories = $this->get_org_categories();

        $filtered_closings = array();
        $orgs = array();
        foreach($closings as $cl) {
            $cl->status = $this->get_closing_status($cl->ID);
            $cl->status_code = $this->get_closing_status($cl->ID, true);
            $cl->note = $cl->post_content;

            $cl->start = get_post_meta($cl->ID, '_closing_start', true);
            $cl->end   = get_post_meta($cl->ID, '_closing_end', true);
            $cl->dayofweek = __(date('l', strtotime($cl->start)));
            
            $terms = wp_get_object_terms($cl->ID, 'closing_org');
            if (count($terms) < 1) {
                $org = new stdClass;
                $org->term_id = "X";
                if (!isset($orgs[$org->term_id])) {
                    $orgs[$org->term_id] = $org;
                    $orgs[$org->term_id]->name = __("Unknown", 'closings');
                    $orgs[$org->term_id]->category = __("None", 'closings');
                    $orgs[$org->term_id]->state    = "XX";
                }
            } else {
                $org = $terms[0];
                if (!isset($orgs[$org->term_id])) {
                    $category = get_tax_meta($org->term_id, $this->prefix.'category');

                    $orgs[$org->term_id] = $org;
                    $orgs[$org->term_id]->category = isset($categories[$category]) ? $categories[$category] : __("None", 'closings');
                    $orgs[$org->term_id]->category_code = $category;
                    $orgs[$org->term_id]->street_address = get_tax_meta($org->term_id, $this->prefix.'street_address');
                    $orgs[$org->term_id]->city     = get_tax_meta($org->term_id, $this->prefix.'city');
                    $orgs[$org->term_id]->state    = get_tax_meta($org->term_id, $this->prefix.'state');
                    $orgs[$org->term_id]->zip      = get_tax_meta($org->term_id, $this->prefix.'zip');
                    $orgs[$org->term_id]->website  = get_tax_meta($org->term_id, $this->prefix.'website');
                    $orgs[$org->term_id]->notes    = wp_unslash(get_tax_meta($org->term_id, $this->prefix.'notes'));
                }
            }

            $cl->org = $orgs[$org->term_id];
            $filtered_closings[] = $cl;
        }

        /*
         * Filter closings
         * 
         * @param array $filtered_closings
         */
        return apply_filters('get_closings', $filtered_closings);
    }

    /**
     * Gets current closings
     *
     * @see Closings::get_closings()
     * @param string $state optional state to get
     * @param array $args optional args to pass to get_posts
     * @return array of array of WP_Post objects
     */
    public function get_closings_by_state($state = null, $args = null) {
        $closings = $this->get_closings($args);
        $closings_by_state = array();

        foreach($closings as $cl) {
            if (empty($cl->org->state)) $cl->org->state = "XX";
            if (!empty($state) && ($state != $cl->org->state)) continue;
            
            if (!isset($closings_by_state[$cl->org->state]))
                $closings_by_state[$cl->org->state] = array('categories'=>array(),'name'=>$this->get_state_name($cl->org->state));
            if (!isset($closings_by_state[$cl->org->state]['categories'][$cl->org->category]))
                $closings_by_state[$cl->org->state]['categories'][$cl->org->category] = array();
            
            $closings_by_state[$cl->org->state]['categories'][$cl->org->category][] = $cl;
        }

        /*
         * Filter state closings
         * 
         * Can be used to add specific groups/categories to their own section
         * 
         * @param array $closings_by_state
         */
        return apply_filters('get_closings_by_state', $closings_by_state);
    }

    /**
     * Helper function for *_privileges functions
     * 
     * @param int | WP_User $user, the WP_User, or id of the user
     * @param string $cap the capability to add or remove
     * @param bool $remove remove the capability, rather than add (option)
     */
    protected function user_cap_helper($user, $cap, $remove = false) {
        if (!($user instanceof WP_User)) $user = new WP_User($user);
        if (!($user instanceof WP_User)) return false;

        if ($remove) return $user->remove_cap($cap);
        else return $user->add_cap($cap);
    }

    /**
     * Makes a user a closings admin
     *
     * @see Closings::user_cap_helper()
     * @see Closings::newsroom_privileges()
     * @see Closings::user_privileges()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @param bool $remove remove the capability, rather than add (option)
     */
    public function admin_privileges($user, $remove = false) {
        if (!$remove) $this->user_privileges($user);
        if (!$remove) $this->newsroom_privileges($user);
        return $this->user_cap_helper($user, "admin_closings", $remove);
    }

    /**
     * Makes a user a closings newsroom user
     *
     * @see Closings::user_cap_helper()
     * @see Closings::admin_privileges()
     * @see Closings::user_privileges()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @param bool $remove remove the capability, rather than add (option)
     */
    public function newsroom_privileges($user, $remove = false) {
        if (!$remove) $this->user_privileges($user);
        return $this->user_cap_helper($user, "close_all_schools", $remove);
    }

    /**
     * Makes a user a closings user
     *
     * @see Closings::user_cap_helper()
     * @see Closings::admin_privileges()
     * @see Closings::newsroom_privileges()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @param bool $remove remove the capability, rather than add (option)
     */
    public function user_privileges($user, $remove = false) {
        return $this->user_cap_helper($user, "close_school", $remove);
    }

    /**
     * Helper function for user_is_* functions
     *
     * @param int | WP_User $user, the WP_User, or id of the user
     * @param string $cap the capability to add or remove
     * @return bool
     */
    protected function user_check_cap_helper($user, $cap) {
        if (!($user instanceof WP_User)) $user = new WP_User($user);
        if (!($user instanceof WP_User)) return new WP_Error('invalid_user', __("Invalid user"));

        return $user->has_cap($cap);
    }

    /**
     * Returns whether a user is a closings admin
     *
     * @see Closings::user_check_cap_helper()
     * @see Closings::user_is_newsroom()
     * @see Closings::user_is_user()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @return bool
     */
    public function user_is_admin($user) {
        return $this->user_check_cap_helper($user, "admin_closings");
    }

    /**
     * Returns whether a user is a closings newsroom user
     *
     * @see Closings::user_check_cap_helper()
     * @see Closings::user_is_admin()
     * @see Closings::user_is_user()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @return bool
     */
    public function user_is_newsroom($user) {
        return $this->user_check_cap_helper($user, "close_all_schools") && (!$this->user_is_admin($user));
    }

    /**
     * Returns whether a user is a closings user
     *
     * @see Closings::user_check_cap_helper()
     * @see Closings::user_is_admin()
     * @see Closings::user_is_newsroom()
     * @param int | WP_User $user, the WP_User, or id of the user
     * @return bool
     */
    public function user_is_user($user) {
        return $this->user_check_cap_helper($user, "close_school") && (!$this->user_is_newsroom($user)) && (!$this->user_is_admin($user));
    }

    /**
     * Returns the user type
     *
     * @see Closings::user_is_admin()
     * @see Closings::user_is_newsroom()
     * @see Closings::user_is_user()
     * @param int | WP_User $user, opttional, the user to check
     * @return string "user", "newsroom", "admin", or "non-user"
     */
    public function get_user_type($user = null) {
        if (is_null($user)) $user = wp_get_current_user();
        if ($this->user_is_user($user)) return "user";
        if ($this->user_is_newsroom($user)) return "newsroom";
        if ($this->user_is_admin($user)) return "admin";
        return "non-user";
    }

    /**
     * Get closing statuses<br>
     * returns an associative array. key is stored, value is displayed<br>
     * the filter closings_statuses can be used to add or remove statuses
     *
     * @return array
     */
    public function get_statuses() {
        $statuses = array(
            "status_00" => __("Closed",'closings'),
            "status_05" => sprintf(__("%d Hour Delay",'closings'), 1),
            "status_10" => sprintf(__("%d Hour Delay",'closings'), 2),
            "status_15" => __("Afternoon/Evening Activities Cancelled",'closings'),
        );
        $statuses['status_X'] = __("Other",'closings');

        /*
         * Filter available statuses
         *
         * To keep consistency, it's recommended that the keys be names
         * status_NN, where NN is a zero-padded integer greater than the current maximum 
         * key It's also recommended you do not replace any of the current statuses,
         * but add (and/or remove) new ones.
         *
         * @param array $statuses
         */
        return apply_filters('closing_statuses', $statuses);
    }

    /**
     * Get org categories<br>
     * returns an associative array. key is stored, value is displayed<br>
     * the filter closing_organization_types can be used to add or remove categories
     *
     * @return array
     */
    public function get_org_categories() {
        $cats = array(
            "cat_00" => __("Public Schools",'closings'),
            "cat_05" => __("Private Schools",'closings'),
            "cat_10" => __("Colleges/Universities",'closings'),
            "cat_15" => __("Federal Government",'closings'),
            "cat_20" => __("Local Government",'closings'),
            "cat_25" => __("Business",'closings'),
            "cat_30" => __("Daycare",'closings'),
            "cat_35" => __("Houses of Worship",'closings')
        );
        $cats['cat_X'] = __("Other",'closings');

        /*
         * Filter available organization types
         *
         * To keep consistency, it's recommended that the keys be named
         * cat_NN, where NN is a zero-padded integer greater than the current 
         * maximum key. It's also recommended you do not replace any of the 
         * current types, but add (and/or remove) new ones.
         *
         * @param array $cats
         */
        return apply_filters('closing_organization_types', $cats);
    }

    /**
     * Get state names<br>
     * returns an associative array. key is two-letter abbrevation, value is name<br>
     * the filter get_states can be used to add or remove categories
     *
     * @return array
     */
    function get_state_name($state_code = null) {
        $states = array(
            "AL" => "Alabama",
            "AK" => "Alaska",
            "AZ" => "Arizona",
            "AR" => "Arkansas",
            "CA" => "California",
            "CO" => "Colorado",
            "CT" => "Connecticut",
            "DE" => "Delaware",
            "DC" => "District of Columbia",
            "FL" => "Florida",
            "GA" => "Georgia",
            "HI" => "Hawaii",
            "ID" => "Idaho",
            "IL" => "Illinois",
            "IN" => "Indiana",
            "IA" => "Iowa",
            "KS" => "Kansas",
            "KY" => "Kentucky",
            "LA" => "Louisiana",
            "ME" => "Maine",
            "MD" => "Maryland",
            "MA" => "Massachusetts",
            "MI" => "Michigan",
            "MN" => "Minnesota",
            "MS" => "Mississippi",
            "MO" => "Missouri",
            "MT" => "Montana",
            "NE" => "Nebraska",
            "NV" => "Nevada",
            "NH" => "New Hampshire",
            "NJ" => "New Jersey",
            "NM" => "New Mexico",
            "NY" => "New York",
            "NC" => "North Carolina",
            "ND" => "North Dakota",
            "OH" => "Ohio",
            "OK" => "Oklahoma",
            "OR" => "Oregon",
            "PA" => "Pennsylvania",
            "RI" => "Rhode Island",
            "SC" => "South Carolina",
            "SD" => "South Dakota",
            "TN" => "Tennessee",
            "TX" => "Texas",
            "UT" => "Utah",
            "VT" => "Vermont",
            "VA" => "Virginia",
            "WA" => "Washington",
            "WV" => "West Virginia",
            "WI" => "Wisconsin",
            "WY" => "Wyoming",
            "AS" => "American Samoa",
            "GU" => "Guam",
            "MP" => "Northern Mariana Islands",
            "PR" => "Puerto Rico",
            "VI" => "Virgin Islands",
            "UM" => "U.S. Minor Outlying Islands"
        );

        /*
         * Filter states
         * 
         * Can be used to add additional state names for the front-end display, or modify default names
         * 
         * @param array $states
         */
        $r = apply_filters('get_states', $states);

        return (!empty($state_code)) ? (!empty($r[$state_code]) ? $r[$state_code] : __('Unknown', 'closings')) : $r;
   }

    /**
     * Set the timezone to be used processing datetimes<br>
     * Defaults to the Wordpress default timzeon
     *
     * @param string $tz a timezone identifier
     * @return bool true if $tz was a valid identifier, false otherwise
     */
    public function set_timezone($tz) {
        $set = false;
        $r = date_default_timezone_set($tz);
        if ($r) {
            $this->timezone = $tz;
            $set = true;
        }
        date_default_timezone_set($this->default_timezone);
        return $set;
    }

    /**
     * Gets the timezone used when processing datetimes.
     *
     * @return string
     */
    public function get_timezone() {
        return $this->timezone;
    }

    /**
     * Gets the default timezone used when processing datetimes.
     *
     * @return string
     */
    public function get_default_timezone() {
        return $this->default_timezone;
    }

    /** @} */
}

/**
 * return Closings->get_closings_by_state
 *
 * @ingroup util
 * @see Closings::get_closings_by_state()
 * @param string $state the two-letter state code, optional
 * @param array $args the arguments to pass to get_posts
 * @return array
 */
function closings_by_state($state = null, $args = null) {
    $c = Closings::get_instance();
    return $c->get_closings_by_state($state, $args);
}

/**
 * returns a Closings object
 *
 * @ingroup util
 * @see Closings::get_instance()
 * @return Closings
 */
function closings_object() {
    return Closings::get_instance();
}


