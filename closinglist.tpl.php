<?php
if (isset($_GET['post']) && is_numeric($_GET['post'])) {
	$prim = array();
	foreach($closings as $key => $cl) {
		if ($cl->ID == $_GET['post']) {
			$cl->primary = true;
			$prim[] = $cl;
			unset($closings[$key]);
		}
	}
	if (!is_null($prim)) {
		$closings = array_merge($prim, $closings);
	}
}
?>
<div class="wrap">
<h2><?php _e('Closings','closings'); $c = Closings::get_instance(); ?>
	<a href="<?php echo admin_url('post-new.php?post_type=closing'); ?>" class="page-title-action add-new-h2"><?php echo $c->post_type->labels->add_new; ?></a>
</h2>
<?php if (isset($_GET['notices']) && count($_GET['notices'])): foreach($_GET['notices'] as $notice): ?>
	<div class="notice is-dismissible below-h2"><p>
		<?php echo $notice; ?>.
	</p></div>
<?php endforeach; endif; ?>
<?php if (!empty($pages_out)) echo $pages_out; ?>
<?php do_action('closing_admin_list', $closings); ?>

<?php foreach($closings as $closing): ?>
<div class="closing<?php if (!!($closing->primary)): ?> primary<?php endif; ?>" id="closing-<?php echo $closing->ID; ?>" data-json="<?php echo esc_attr(json_encode($closing)); ?>">
	<h2 class="title">
		<?php if ($closeall): ?>
			<span class="org org-<?php echo $closing->org->slug; ?>"><?php echo $closing->org->name; ?></span>: 
		<?php endif; ?>
		<?php echo $closing->status; ?>
		<?php echo $closing->dayofweek; ?>
	</h2>
		<?php if (!empty($closing->note)): ?>
			<p class="notes"><?php _e('Note','closings') ?>: <?php echo $closing->note; ?></p>
		<?php endif; ?>
		<p class="post"><?php _e('Posted','closings') ?>: <?php echo date('D, M j g:i A', strtotime($closing->post_date)); ?></p>
		<p class="start"><?php _e('Begins','closings') ?>: <?php echo date('D, M j g:i A', strtotime($closing->start)); ?></p>
		<p class="end"><?php _e('Expires','closings') ?>: <?php echo date('D, M j g:i A', strtotime($closing->end)); ?></p>
		<?php if (!empty($closing->ID)): ?>
			<p class="actions">
				<a href="<?php echo esc_attr(admin_url('post.php?action=edit&post=' . $closing->ID)); ?>" class="add-new-h2"><?php _e('Edit Closing','closings') ?></a>
				<?php if ($c->user_is_admin(wp_get_current_user())): ?>
					<a href="<?php echo esc_attr(wp_nonce_url(admin_url('admin.php?page=closings-admin&show=expire&post=' . $closing->ID),'expire-closing')); ?>" class="add-new-h2"><?php _e('Expire Closing Early','closings') ?></a>
				<?php endif; ?>
			</p>
		<?php endif; ?>
</div>
<?php endforeach;?>
<?php do_action('after_closing_admin_list', $closings); ?>
</div>
