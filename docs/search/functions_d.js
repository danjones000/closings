var searchData=
[
  ['user_5fcap_5fhelper',['user_cap_helper',['../group__util.html#ga34104af0e1d060157df58cfa0a0090d1',1,'Closings']]],
  ['user_5fcheck_5fcap_5fhelper',['user_check_cap_helper',['../group__util.html#ga8730824f34c60a58437ba1019b15689d',1,'Closings']]],
  ['user_5ffields',['user_fields',['../group__wphooks.html#ga2fef1c1cb696723aed56a5942b97c7e5',1,'Closings']]],
  ['user_5ffields_5fupdate',['user_fields_update',['../group__wphooks.html#ga1c45b27d22681d912ec67b66acaadb77',1,'Closings']]],
  ['user_5fis_5fadmin',['user_is_admin',['../group__util.html#gaa900ec87625eb00d4b38224921b45d56',1,'Closings']]],
  ['user_5fis_5fnewsroom',['user_is_newsroom',['../group__util.html#gaeeb46f34dde18f04149be826cf59a994',1,'Closings']]],
  ['user_5fis_5fuser',['user_is_user',['../group__util.html#gafc2297699cb182263b73e06e0709da5b',1,'Closings']]],
  ['user_5fprivileges',['user_privileges',['../group__util.html#ga0b346cd6f1732139fcef36cffc11716d',1,'Closings']]]
];
