var searchData=
[
  ['activate_5fplugin',['activate_plugin',['../group__wphooks.html#ga29e236ed2a381db7e2c56b6db2d8433b',1,'Closings']]],
  ['add_5fadmin_5fmenus',['add_admin_menus',['../group__wphooks.html#ga0c57a077a3d8159ab516fd458217d352',1,'Closings']]],
  ['add_5fclosing_5forg',['add_closing_org',['../group__init.html#ga9990a9a51c59b962d8dcd1f976b3e8b1',1,'Closings']]],
  ['add_5fclosing_5fpost_5ftype',['add_closing_post_type',['../group__init.html#ga560a74e0d7de63dd4e1d0029c8202add',1,'Closings']]],
  ['add_5fclosing_5fstatus',['add_closing_status',['../group__init.html#ga4348e100cbbb3a42da48b343fa74dcec',1,'Closings']]],
  ['add_5fform',['add_form',['../group__wphooks.html#ga2d71b2d6645881248fe767d3ba1c9c53',1,'Closings']]],
  ['add_5fform_5fscript',['add_form_script',['../group__wphooks.html#ga152ba4a8a1bf213ef394299312776025',1,'Closings']]],
  ['add_5fnew_5forg',['add_new_org',['../group__util.html#gacc5fe832f199d48547faf038881df194',1,'Closings']]],
  ['admin_5finit',['admin_init',['../group__wphooks.html#ga2f10fbf7bae00817511bc210bd76bb17',1,'Closings']]],
  ['admin_5fpage_5flinks',['admin_page_links',['../group__template.html#gac5620062de66242f804f8081ee8c12fc',1,'Closings']]],
  ['admin_5fprivileges',['admin_privileges',['../group__util.html#ga6c98edb472be2b132edcb57279117cf3',1,'Closings']]],
  ['ajax_5fimport',['ajax_import',['../group__wphooks.html#gabcf45a197e9d097f466b2845ea913d77',1,'Closings']]]
];
