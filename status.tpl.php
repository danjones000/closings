<script type="text/javascript">
window.closings_user_type = <?php echo json_encode(closings_object()->get_user_type()); ?>;
</script>
<div class="hidden notice error" id="closing-org-missing-error"><p>
	<?php _e("Please choose a closing organization",'closings'); ?>.
</p></div>
<?php wp_nonce_field('add_closing', '_closings_nonce'); ?>
<?php do_action('before_closings_edit_table'); ?>
<table class="form-table">
<tbody>
<?php do_action('before_closings_edit'); ?>

<tr>
  <th scope="row"><label for="closing_status"><?php _e('Status', 'closings'); ?></label></th>
  <td><select id="closing_status" name="closing_status">
	  <?php foreach($statuses as $key => $value): ?>
	  <?php $s = ($key == $status) ? 'selected="selected"' : ''; ?>
	  <option <?= $s ?> value="<?= $key ?>"><?php _e($value, 'closings'); ?></option>
	  <?php endforeach; ?>
	</select>
  </td>
</tr>
<tr>
	<th scope="row"><label for="closing_day"><?php _e('Date','closings'); ?></label></th>
	<td><input type="date" name="closing_day" id="closing_day" value="<?= esc_attr($day); ?>" class="regular-text" /></td>
</tr>
<tr>
  <th scope="row"><label for="closing_notes"><?php _e('Notes','closings'); ?></label></th>
  <td><input type="text" value="<?= esc_attr($notes) ?>" id="closing_notes" name="closing_notes" class="regular-text" /></td>
</tr>
<?php do_action('after_closings_edit'); ?>

</tbody>
</table>
<?php do_action('after_closings_edit_table'); ?>
