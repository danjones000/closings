# Closings

This plugin creates a system to show school closings on your Wordpress
site. This is particularly useful for news organizations who wish to allow
schools, government, and other organizations to easily display on their website
when their organization is closed. The system includes a login and permissions
system to allow organizations to create closings for themselves, but also allows
for select users within the organization to create closings.

## Installation

1. Upload the `closings` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Select "Closings" in the sidebar and begin adding organizations.

## Architecture

This plugin creates custom post types, custom taxonomies, a new user role, and
new user capabilities.

### Closing Post Type

The `closing` post type is the basis for this plugin. For each new closing, the
user can choose a status (e.g., Closed, Delayed), and a date. The user can enter
notes (which is actually stored as the post content), and can choose an
organization for the closing (more on that later). A title is auto-generated
based on the status, organization, etc. Depending on your theme, a page that
displays this may be visible, and an archive page per organization may also be 
visible. It is up to the theme author to customize the look of these pages (or
not use them at all). Based on the day and status, a start and end time for each
closing is stored. The end time determines when the closing expires (see below),
and the start time determines the sorting of the closings.

#### Custom status: Expired

In order to keep track of old closings, an additional status of "expired" has
been added. When the end time has reached for a closing, the status changes to
"expired". This is done via WP-Cron.

### Closing Organization Taxonomy

Each `closing` must belong to one, and only one, `closing_org` taxonomy. There
are methods in place to ensure that this happens. The `closing_org` contains
additional metadata that probably won't be visible in the theme, but will be
useful for contacting the organization in the future.

### Roles and Capabilities

#### Closings User (Role)

When adding an organization to the system, it is usually desirable to add a user
who is able to close that organization. E.g., if the organization is a school
district, it would be useful for a district administrator (or her secretary) to
close the organization herself.

A new user role (`closings-user`) was created. This role has the capability
`close_school` to allow them to close a single organization. When a new
organization is added via the closings admin (or via `add_org`), a Closings User
may be created that can create closings for that `closing_org`. A
`closings-user` may only create `closing`s for a single `closing_org`.

#### Newsroom user (capability)

Current users within your Wordpress installation can be made "newsroom" users in
the closings system. These users have a capability of `close_all_schools`. This
allows them to create closings for any organization. Additionally, by default,
they can also add new `closing_org`s to the system, as well as new
`closings-user`s, and modify the metadata for the `closing_org`s. This can be
changed using the `closings_newsroom_can_add` filter (see below).

It's recommended that if you need to add new users who need to have newsroom
access to the closings system, but no other access to Wordpress, then they
should be added as Subscribers, and then given newsroom access.

#### Admin user (capability)

An additional capability, `admin_closings`, is added for users who should be
able to administer the system. When the plugin is initialized, all current
Wordpress Administrators are given the `admin_closings` capability. These can be
removed, and additional closing admins can be added. In addition to all the
abilities of newsroom users, admins can modify users (create new admin/newsroom
users, remove capabilities), import, and export organizations.

## Showing on the site

### Shortcodes

The plugin provides two shortcodes: `[closings]` and
`[closings_by_state]`. `[closings]` outputs a table listing all closings in
order of closing start time (sorting can be customized with a filter, see
below). `[closings_by_state]` outputs multiple tables and sections. There is a
section for each state, and within each section, a table for each organization
type.

### Theme functions

The plugin provides a single function (`closings_by_state`) to get all the
closings in order to output in a theme. This returns a multi-dimensional array,
in the following form:

    array(
        '<state code>' =>
	    array(
		    'name' => '<state name>',
		    'categories' =>
		    array(
			    '<type of org>' =>
			    array(<closings>...)
            )
        )
    )

This can be used to output the closings in a manner similar to the
`[closings_by_state]` shortcode.

In addition to this function, the Closings class provides several more functions
that may be useful to outputting closing information on the site. To call this
functions, you must first get an instance of the class (Closings is a
[Singleton](https://en.wikipedia.org/wiki/Singleton_pattern) class), and call
methods from this instance. There are two ways to get the instance. First is a
static method, which can be invoked thus:

    $closings = Closings::get_instance();

Alternately, a helper function is available, which can be invoked thus:

    $closings = closings_object();

There is no difference between this two methods. For the purpose of
demonstration, I'll using `closings_object`. In all of the examples below
`closings_object()->method()` could be replaced with `$closings = closings_object(); $closings->method()`,
so that if the Closings object is used multiple times, `closings_object()`
needn't be called every time.

#### Methods

`closings_object()->get_closings($args)`: This returns an array of closings, which
can be used to output closings in a way similar to the `[closings]`
shortcode. This method uses the Wordpress `get_posts` function. You can pass the
same arguments to `get_closings` that you would pass to `get_posts`.

`closings_object()->get_closings_by_state($state, $args)`: This is the same as
`closings_by_state()`. In fact `closings_by_state()` simply calls this
method. If a `$state` is supplied, only that state (a two-letter code should be
used) will be returned. `$args` will be passed to `get_closings`.

`closings_object()->user_is_admin($user)`: If you would like to display certain
information on the frontend only to users of the system, you can use this
function to check if the user is a closings admin. `$user` can be the Wordpress
function `wp_get_current_user()`. Additionally, `user_is_newsroom` and
`user_is_user` is available.

`closings_object()->get_user_type($user)`: This will return the `$user`'s user
type as a string, which will be "admin", "newsroom", "user", or "non-user".

`closings_object()->get_org_status($org)`: This can get the status of a single
organization. `$org` may be a `term_id`, a slug, or an object such as the
returned by `get_terms`. This returns a single `closing` post, which represents
the next upcoming status for the given `$org`. If there are no upcoming
`closing`s, an object with the status of "Open" is returned.

`closings_object()->get_current_org_status($org, $ts)`: Similar to
`get_org_status`, except that it returns the status for a given time. `$ts` is
optional, and defaults to "now". `$ts` may be a Unix timestamp, or a date
string, such as "today", "yesterday", or "tomorrow", as long as it's recognized
by PHP's `strtotime` function.

`closings_object()->get_orgs($args)`: This returns all the organizations. It
uses Wordpress's `get_terms` function, and may take an array that will be passed
to that function.

`closings_object()->get_org_by($field, $value)`: This is similar to `get_orgs`,
however it returns only a single `closing_org` and utilizes Wordpress's
`get_term_by` function. The `$field` and `$value` are the same as what should be
passed to `get_term_by`.

`closings_object()->get_orgs_by($field, $values)`: This is similar to
`get_org_by`, however it accepts an array of `$value`s and returns an array of
`closing_org`s.

`closings_object()->get_user_org($user)`: This gets the `closing_org` assigned
to a particulr `closings-user`.

More information about the methods available can be found in the
[API docs](docs/).

## Customization

Most of the customization is done through actions and filters. Nearly every
aspect of the plugin can be modified using the appropriate hook.

### Actions

`before_closings_init`: This is the best place to add in your own filters and
actions. This fires before styles are registered, the custom post type
(`closing`) is registered, before the custom taxonomy (`closing_org`) is
registered, and so on. The closings object is passed as a variable.

`after_closings_init`: This fires after all those things mentioned in
`before_closings_init` occurs. The closings object is passed as a variable.

`closing_admin_page_{show}`: This allows you to create your own admin pages. For
example, you could have a custom admin print view for your closings called
`printview`. You would, therefore, call `add_action("closing_admin_page_printview",
"my_printview")`. In `function my_printview`, you would output the HTML for that
page. You would also need to use the `closing_admin_pages_header` filter
described below. `$pages_out` is passed to the hook to allow you to output the
admin page links on your custom page.

`closing_admin_page_default`: Called on the admin page if an unknown page is
requested, and no `closing_admin_page_{show}` is called. `$show` is passed to
the hook, as well as `$pages_out`.

`closings_new_org_after_user_insert`: Called every time a new closings_user is
added. Variables passed to the hook are: `$user` (the new user, with the new
password in `$user->password`), and `$args` (the arguments passed to the
`add_new_org` method). Note that this is called after the user has been added to
the system, but before the new organization (taxonomy) has been created.

`closings_new_org_after_term_insert`: This is similar to the previous action,
but for the new organization. Variables are `$org` (the new taxonomy), and
`$args`.

`closings_add_org`: Fires after `add_new_org` completes, regardless of
successful completion. This function gets one variable (`$r`). If errors
occurred, `$r` is a WP_Error object with relevant information. If no errors
occurred, `$r` is an array of the following form: `array('user' => $user, 'org' => $org)`.

### Filters

`closings_newsroom_can_add`: This determines if `newsroom` users can add new
organizations. This defaults to true. If you only want admin users to be able to
add new organizations, simply add the following code to your `functions.php` (or
a custom plugin):

    add_filter("closings_newsroom_can_add", "__return_false");

`filter_closing_start_end`: This allows you to create custom statuses with
custom start and end times. This function should return an array of two
variables, the first of which is the start time, and second is the end time (in
the form of `YYYY-MM-DD`). Additional variables are `$status` (the status code
for the closing, such as `status_00`), and `$day` (the day of the closing, in
the form of `YYYY-MM-DD`). Please keep in mind that the default PHP timezone set
on the server may not match the Wordpress timezone.

`closings_new_org`: Used when adding a new organization via the admin panel. It
should return the array which will be passed to `add_new_org`. It is also given
the current $_POST (although this can still be obtained the usual way).

`closings_org_meta`: This are meta fields to add to each `closing_org`. It uses
the [Tax-Meta Class](https://github.com/bainternet/Tax-Meta-Class). It should
return a Tax_Meta_Class object. It is also passed the prefix used for the meta
fields. To add a "flagged" field, e.g., do the following in your function:

    function custom_closing_org_meta($meta, $prefix) {
        $meta->addCheckbox($prefix.'flagged',array('name'=> __('Flagged')));
    }

`closing_admin_pages_header`: This is how you add custom admin pages (or remove
default ones). It is given an 2D array. To add new pages, simply append to the
array like so:

    function custom_admin_page($pages) {
        $pages[] = array('label'=>__("Print View"), 'url' => add_query_arg(array('page' => 'closings-admin', 'show' => 'printview'), admin_url('admin.php')));
        return $pages;
    }

Then make sure to use the `closing_admin_page_printview` action as described
above.

`closing_export_fields`: This is used to control which fields will be included
in an export. Simply append to the array with whatever fields from the
organization or user that you would like also included in the CSV file.

N.B.: There is no way to get the user passwords in the export, as Wordpress does
not store plaintext passwords.

`closing_export_organization`: This can be used to filter the organization
before it's sent to the CSV. You probably don't need this, unless you want to
customize how specific fields appear. Do not delete or add elements to the
array, as that will make an invalid CSV file. You should only modify existing
elements.

`get_closing_org`: This will customize the organization object that is returned
by `get_org*` methods. This is useful especially if you've added new
fields. E.g., to add the "Flagged" field from above, you might do the following:

    public function closing_org_filter($org) {
        $org->flagged = get_tax_meta($org->term_id, closings_object()->prefix."flagged");
        $org->flagged = !empty($org->flagged);

        return $org;
    }

`closing_orgs`: This will filter organizations returned by `get_orgs` or
`get_orgs_by`. You probably don't need this, as the `get_closings_org` filter
has already run on each of them.

`get_closings`: Filter closings returned by `get_closings`. With this, you can
sort closings, remove undesired closings, etc.

`get_closings_by_state`: Filter closings returned by
`get_closings_by_state`. Please note that the `get_closings` filter has already
run on these.

`closing_statuses`: Add/remove/modify statuses. The format is associative array
where the keys are a status code (usually in the format of `status_XX`), and the
value is the display name (Such as "Closed", or "2 Hour Delay"). It's
recommended that the current keys are not overwritten with a different status
(e.g., `status_05` is currently "1 Hour Delay". That shouldn't be changed to
something unrelated, such as "Closing Early").

`closing_organization_types`: Add/remove/modify organization types. Similar to
`closing_statuses`, this is an associative array.

`get_states`: This filters state names that correspond to two-letter state
codes. It has an associate array where the key is the two-letter code (e.g.,
"AK"), and the value is the state name (e.g., "Alaska"). With no filter, it
contains all fifty United States, and all (hopefully) US territories, etc. These
are used for matching a state code to a state name for `get_closings_by_state`
and displaying on the site.

## Frequently Asked Questions

### How do I request new features?

TBD
