var tagBox;

(function($) {

	tagBox.origFlushTags = tagBox.flushTags;

	tagBox.flushTags = function(el, altEl, f) {
		var tagsval, newtags, text, tagsToCheck, tagsToAdd, 
			tags = $( '.the-tags', el ),
			newtag = $( 'input.newtag', el ),
			comma = window.tagsBoxL10n ? window.tagsBoxL10n.tagDelimiter : ",",
			tax = $(el).closest('div.tagsdiv').attr('id');

		if (tax != "closing_org") return this.origFlushTags(el, altEl, f);
		altEl = altEl || false;
		text = altEl ? $(altEl).text() : newtag.val();

		if ( 'undefined' == typeof( text ) ) {
			return false;
		}

		tagsval = tags.val();
		newtags = tagsval ? text + comma + tagsval : text;

		newtags = this.clean( newtags );
		newtags = array_unique_noempty( newtags.split( comma ) ).join( comma );

		tagsToCheck = newtags.split(',');
		tagsToAdd = []

		for (var i = 0; i < tagsToCheck.length; i++) {
			var t = tagsToCheck[i];
			var args = {url:ajaxurl + '?action=ajax-tag-search&tax=' + tax + '&q=' + t};
			args.success = function(data, status, xhr) {
				window.gotit = !!data ? data.split('\n') : false;
				if (gotit) tagsToAdd.push(gotit[0]);
			};
			args.async = false;
			$.ajax(args);
			if (tagsToCheck.length > 0) break;
		}

		newtags = tagsToAdd.join(',');
		tags.val( newtags );
		this.quickClicks( el );

		if ( ! altEl )
			newtag.val('');
		if ( 'undefined' == typeof( f ) )
			newtag.focus();

		return false;
	};

	$(document).ready(function() {
		window.postForm = $('#post');
		if (window.closings_user_type == "admin" || window.closings_user_type == "newsroom") {
			$('#post').on('submit', function(evt) {
				var taxInputClosingOrg = $('#tax-input-closing_org');
				if (taxInputClosingOrg.length > 0) {
					if (!(taxInputClosingOrg.val().trim())) {
						$('#closing-org-missing-error').removeClass('hidden');

						var toScrollTo = $("#closing-org-missing-error").offset().top - $('#wpadminbar').height() - 5;
						if ($(window).scrollTop() > toScrollTo) {
							$('html, body').animate({
								scrollTop: toScrollTo
							}, 200);
						}

						evt.preventDefault();
					}
				}
			});
		}
	});

}(jQuery));
