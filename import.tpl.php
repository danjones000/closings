<div class="wrap">
<h2><?php _e('Import / Export','closings'); ?></h2>
<?php if (!empty($pages_out)) echo $pages_out; ?>
<h3><?php _e('Export','closings'); ?></h3>
<p>
	<?php printf(__('Please <a href="%s">click here</a> to download a CSV of your organizations.', 'closings'), admin_url('admin.php?page=closings_export')); ?>
	<?php _e("This file may be imported into another Wordpress installation with this plugin.", 'closings') ?>
</p>

<h3><?php _e('Import','closings'); ?></h3>

<?php if (!$upload): ?>
	<p><?php _e("Upload your CSV file and we'll import the users and organizations.", 'closings'); ?></p>
	<?php wp_import_upload_form($_SERVER['REQUEST_URI']); ?>

	<p><?php _e("Your CSV file <strong>must</strong> contain the following columns", 'closings'); ?>:</p>
	<ul class="closings_req_list">
		<li><code>email</code></li>
		<li><code>org_name</code></li>
	</ul>
	<p><?php _e("It may also include the following columns", 'closings'); ?>:</p>
	<ul class="closings_req_list">
		<li><strong><code>user_login</code></strong></li>
		<li><strong><code>pass</code></strong></li>
		<li><code>first_name</code></li>
		<li><code>last_name</code></li>
		<li><code>phone</code></li>
		<li><code>alt_name</code></li>
		<li><code>alt_email</code></li>
		<li><code>alt_phone</code></li>
		<li><code>street_address</code></li>
		<li><code>city</code></li>
		<li><code>state</code></li>
		<li><code>zip</code></li>
		<li><strong><code>category</code></strong></li>
		<li><code>website</code></li>
		<li><code>notes</code></li>
	</ul>
	<p><?php _e("If no user_login is included, the domain portain of the email will be used. If no password is included, a random password will be generated. The category should match one of the category names. If no category is chosen, or an invalid category is chosen, \"Other\" will be chosen.", 'closings'); ?></p>
	<p><?php _e("Any other columns will be ignored.", 'closings'); ?></p>
<?php else: ?>
	<?php if (is_wp_error($new_orgs)): ?>
		<p><?php _e("There were errors processing your file.", 'closings'); ?></p>
		<?php foreach($new_orgs->get_error_codes() as $error_code): ?>
			<p class="error" id="error-<?php echo $error_code; ?>">
				<?php echo $new_orgs->get_error_message($error_code); ?></p>
		<?php endforeach; ?>
		<p><?php _e("Ensure that the file is a CSV file with the correct headers.", 'closings'); ?></p>
		<?php echo $t; ?>
	<?php else: ?>
		<?php if ($ajax): ?>
			<script type="text/javascript">
			(function($) {
				var filebase = <?php echo json_encode($filebase); ?>;
				var total = <?php echo json_encode($total); ?>;
				var request = function(number, filebase, total) {
					if (number > total) return;
					data = {
						action: "import_closings",
						filename: filebase + number,
					}
					$.post(ajaxurl, data, function(resp) {
						arguments.callee(number + 1, filebase, total);
					})
				}
				request(1, filebase, total);
			})(jQuery);
			</script>
		<?php else: ?>
			<p><?php printf(__("Successfully imported %s.", 'closings'), $filename); ?></p>
			<?php foreach($new_orgs as $org): ?>
				<div class="closing-org">
					<?php if (!is_wp_error($org['return'])): ?>
						<h2 class="title"><?php echo $org['org_name']; ?></h2>
						<p class="notes"><?php printf(__("Successfully <a href=\"%s\">added</a> organization", 'closings'), admin_url('edit-tags.php?action=edit&taxonomy=closing_org&post_type=closing&tag_ID=' . $org['return']['org']->term_id)); ?></p>
						<p class="user"><?php printf(__("Login created for <a href=\"%s\">%s</a>.", 'closings'), admin_url('user-edit.php?user_id=' . $org['return']['user']->ID), $org['user_login']); ?></p>
					<?php else: ?>
						<h2 class="title"><?php printf(__("Failed to properly add %s.", 'closings'), $org['org_name']) ?></h2>
						<?php foreach($org['return']->get_error_codes() as $error_code): ?>
							<p class="error" id="error-<?php echo $error_code; ?>">
								<?php echo $org['return']->get_error_message($error_code); ?></p>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>	
	<?php endif; ?>

<?php endif; ?>

</div>
