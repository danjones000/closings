<?php

// init action
add_action('after_closings_init', 'closings_extra_initialization');
function closings_extra_initialization() {
    $c = Closings::get_instance();
    // do additional initialization
}

// Adding a new closing status
add_filter('closing_statuses', 'custom_closing_statuses');
function custom_closing_statuses($statuses) {
    $statuses['status_18'] = "Close early";
}
// With appropriate start and end times
add_filter('filter_closing_start_end', 'custom_closing_status_start_end_time');
function custom_closing_status_start_end_time($startend, $status, $day) {
    list($start, $end) = $startend;

    if ($status == "status_18") {
        $start = $day . "T14:00";
        $end = "$day" . "T23:59";
    }

    return array($start, $end);
}


// Adding additional states
add_filter('get_states', 'states_filter');
function states_filter($states) {
    $states["FG"] = "Federal Government";
    return $states;
}

// Add metadata to organizations, in this case a "flag", which could be used with other filters (e.g., for sorting)
add_filter('closings_org_meta', 'add_closings_org_flag', 10, 2);
function add_closings_org_flag($meta, $prefix) {
    $c = Closings::get_instance();

    // Only for newsroom users. We don't want the organizations to know they're "flagged"
    $me = wp_get_current_user();

    if ($c->user_is_admin($me) || $c->user_is_newsroom($me))
        $meta->addCheckbox($prefix.'flagged',array('name'=> __('Flagged')));

    return $meta;
}

// Add new organization types, and sort the list
add_filter('closing_organization_types', 'closing_add_fed_other');
function closing_add_fed_other($cats) {
    $cats["cat_17"] = __("Federal Government/Other",'closings');
    ksort($cats);
    return $cats;
}

// Modify closing list, in this case, sort by start datetime
add_filter('get_closings', 'closings_sort_by_start_filter');
function closings_sort_by_start_filter($closings) {
    usort($closings, 'closings_sort_by_start');
    return $closings;
}
function closings_sort_by_start($a, $b) {
    // First we want our "flagged" orgs to appear first (see add_closings_org_flag above)
    $c = Closings::get_instance();
    $a->org->flagged = get_tax_meta($a->org->term_id, $c->prefix."flagged");
    $b->org->flagged = get_tax_meta($b->org->term_id, $c->prefix."flagged");
    $a->org->flagged = !empty($a->org->flagged);
    $b->org->flagged = !empty($b->org->flagged);
    
    if ($a->org->flagged && !$b->org->flagged) return -1;
    if ($b->org->flagged && !$a->org->flagged) return 1;

    // If neither are flagged, or both are flagged, sort by start time, earliest first
    $time = strtotime($a->start) - strtotime($b->start);
    if ($time != 0) return $time;

    // If same time, sort by orgs alphabetically
    return strcmp($a->org->name, $b->org->name);
}

// Modify closing list by state, in this case, prefering DC metro area
add_filter('get_closings_by_state', 'closings_sort_for_dc_filter');
function closings_sort_for_dc_filter($closings) {
    uksort($closings, 'dc_states_sort');
    return $closings;
}
function dc_states_sort($a, $b) {
    $states = array("XX" => 0, "WV" => 1, "DC" => 2, "MD" => 4, "VA" => 8, "FG" => 16);
    if (!isset($states[$a]) && !isset($states[$b])) return strcmp($a, $b);
    
    if (!isset($states[$a])) $a = "XX";
    if (!isset($states[$b])) $b = "XX";
    return $states[$b] - $states[$a];
}

// Adding addition admin pages
// We're going to add a "Print View" for the admin side. First let's create the page
add_action('closing_admin_page_printview', 'do_admin_printview');
function do_admin_printview($pages_out) {
    // The "Print View" will have the current status of all flagged orgs
    $c = Closings::get_instance();
    $orgs = $c->get_orgs();
    $closings = array();
    
    foreach($orgs as $org) {
        $org->flagged = get_tax_meta($org->term_id, $c->prefix."flagged");
        $org->flagged = !empty($org->flagged);
        if (!$org->flagged) continue; // Skip non-flagged orgs

        $closings[] = $c->get_org_status($org);
    }
    
    $closeall = true;
    require_once(CLOSINGS_PLUGIN_DIR . '/closinglist.tpl.php');
}
// Now we have to add a menu item for the printview
add_filter('closing_admin_pages_header', 'add_closings_pages');
function add_closings_pages($pages) {
    $pages[] = array('url' => add_query_arg(array('page' => 'closings-admin', 'show' => 'printview'), admin_url('admin.php'))
                     , 'label' => 'Print View');
    return $pages;
}

// And a default admin page for undefined pages
add_action('closing_admin_page_default', 'do_admin_default', 10, 2);
function do_admin_default($action, $pages_out) {
    echo "<div class=\"wrap\"><h2>What?</h2>";
    echo $pages_out;
    echo "<p>You tried to do $action, but I don't know how to do that.</p>\n</div>\n";
}

// Hook to send email when new closing is created
add_action('save_post_closing', 'notify_new_closing');
function notify_new_closing($post_id) {
    $c = Closings::get_instance();
    $close = $c->get_closings("include=$post_id")[0];
    $message = $close->org->name . " created a new closing.\n";
    $message .= $close->status . " " . $close->dayofweek . "\n";
    $message .= $close->note;
    wp_mail('admin@example.com', "New closing for " . $close->org->name, $message);
}
