<?php
/*
Plugin Name: Closings
Plugin URI: https://gitlab.hubbardradio.com/djones/closings/
Description: Wordpress system to display and manage school (and other organization) closings
Version: 0.0.1
Author: Dan Jones
Author URI: http://wtop.com/
License: Copyright 2015 WTOP
Text Domain: closings
*/

define('CLOSINGS_VERSION', '0.0.1');
define('CLOSINGS_PLUGIN', __FILE__);
define('CLOSINGS_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CLOSINGS_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once(CLOSINGS_PLUGIN_DIR . 'Tax-meta-class/Tax-meta-class.php');
require_once(CLOSINGS_PLUGIN_DIR . 'class.closings.php');
$closings = Closings::get_instance();

// WP-CLI Integration
if (defined('WP_CLI') && WP_CLI)
    require('class.wp_cli.php');
