<div class="wrap">
<h2><?php echo __('Closings Administration','closings'); ?></h2>
<?php if (is_array($notices)): foreach($notices as $n): ?>
	<?php if (empty($n['class'])) $n['class'] = ''; ?>
	<?php if (empty($n['text'])) $n['text'] = ''; ?>
	<div class="notice is-dismissible below-h2 <?php echo esc_attr($n['class']); ?>"><p>
		<?php echo $n['text']; ?>
	</p></div>
<?php endforeach; endif; ?>
<?php do_action('closings_after_title'); ?>

<?php echo $pages_out; ?>

<h3><?php _e('Add Closings User and Organization','closings'); ?></h3>

<form method="post">
  
  <h4><?php _e('User Settings','closings'); ?></h4>
  <?php do_action( 'user_new_form_tag' ); ?>
  <input name="action" type="hidden" value="addorg" />
  <?php wp_nonce_field('add_closings_org', '_wpnonce_add_closing_org'); ?>
  
  <table class="form-table">
	<tbody>
	  <tr class="form-field form-required">
		<th scope="row"><label for="user_login"><?php _e('Username'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
		<td><input placeholder="username" name="user_login" type="text" id="user_login" value="<?php echo esc_attr($new_user_login); ?>" aria-required="true" /></td>
	  </tr>
	  <tr class="form-field form-required">
		<th scope="row"><label for="email"><?php _e('E-mail'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
		<td><input placeholder="jsmith@example.edu" name="email" type="email" id="email" value="<?php echo esc_attr($new_user_email); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="first_name"><?php _e('First Name') ?> </label></th>
		<td><input placeholder="Jane" name="first_name" type="text" id="first_name" value="<?php echo esc_attr($new_user_firstname); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="last_name"><?php _e('Last Name') ?> </label></th>
		<td><input placeholder="Smith" name="last_name" type="text" id="last_name" value="<?php echo esc_attr($new_user_lastname); ?>" /></td>
	  </tr>
	  <tr class="form-field form-required">
		<th scope="row"><label for="pass1"><?php _e('Password'); ?> <span class="description"><?php /* translators: password input field */_e('(required)'); ?></span></label></th>
		<td>
		  <input class="hidden" value=" " /><!-- #24364 workaround -->
		  <input name="pass1" type="password" id="pass1" autocomplete="off" />
		</td>
	  </tr>
	  <tr class="form-field form-required">
		<th scope="row"><label for="pass2"><?php _e('Repeat Password'); ?> <span class="description"><?php /* translators: password input field */_e('(required)'); ?></span></label></th>
		<td>
		  <input name="pass2" type="password" id="pass2" autocomplete="off" />
		</td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="phone"><?php _e('Phone Number'); ?></label></th>
		<td><input placeholder="(939) 555-3226" name="phone" id="phone" type="text" class="code" value="<?php echo esc_attr($phone); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="alt_name"><?php _e('Alternate Contact Name', 'closings'); ?></label></th>
		<td><input placeholder="Jim Everyman" name="alt_name" id="alt_name" type="text" class="code" value="<?php echo esc_attr($alt_name); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="alt_email"><?php _e('Alternate E-Mail', 'closings'); ?></label></th>
		<td><input placeholder="jim@everywhere.com" name="alt_email" id="alt_email" type="email" class="code" value="<?php echo esc_attr($alt_email); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="alt_phone"><?php _e('Alternate Phone Number', 'closings'); ?></label></th>
		<td><input placeholder="(636) 764-8437" name="alt_phone" id="alt_phone" type="text" class="code" value="<?php echo esc_attr($alt_phone); ?>" /></td>
	  </tr>
	</tbody>
  </table>
  <?php do_action('user_new_form', 'add-existing-user'); ?>

  <h4><?php echo __('Organization Settings','closings'); ?></h4>
  <table class="form-table">
	<tbody>
	  <tr class="form-field form-required">
		<th scope="row"><label for="org_name"><?php _e('Organization Name','closings'); ?>
			<span class="description"><?php _e('(required)'); ?></span>
		</label></th>
		<td><input placeholder="<?php esc_attr_e('Springfield ISD', 'closings') ?>" name="org_name" id="org_name" type="text" class="code" value="<?php echo esc_attr($org_name); ?>" /></td>
	  </tr>
	  <tr class="form-field form-required">
		<th scope="row"><label for="org_cat"><?php _e('Category','closings'); ?></label></th>
		<td><select name="org_cat" id="org_cat">
			<?php foreach($categories as $key => $cat): ?>
				<option value="<?php echo esc_attr($key); ?>"><?php echo $cat; ?></option>
			<?php endforeach; ?>
		</select></td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="org_street_address"><?php _e('Street Address'); ?></label></th>
		<td><input placeholder="<?php esc_attr_e('123 Fake St','closings') ?>" name="org_street_address" id="org_street_address" type="text" class="code" value="<?php echo esc_attr($org_street_address); ?>" /></td>
	  </tr>
	  <tr class="">
		<th scope="row"><label for="city"><?php _e('City'); ?></label></th>
		<td>
			<input placeholder="<?php esc_attr_e('Washington','closings') ?>" name="city" id="city" type="text" class="regular-text" value="<?php echo esc_attr($city); ?>" />
			<label for="state"><strong><?php _e('State/Province'); ?></strong></label>
			<input placeholder="DC" name="state" id="state" type="text" size="3" class="small-text" value="<?php echo esc_attr($state); ?>" />
			<label for="zip"><strong><?php _e('Zip/Postal Code'); ?></strong></label>
			<input placeholder="12345" name="zip" id="zip" type="text" size="5" class="small-text" value="<?php echo esc_attr($zip); ?>" />
		</td>
	  </tr>
	  <tr class="form-field">
		<th scope="row"><label for="website"><?php _e('Website') ?></label></th>
		<td><input placeholder="http://www.example.edu/" name="website" type="url" id="website" class="code" value="<?php echo esc_attr($website); ?>" /></td>
	  </tr>
	  <tr class="form-field">
		  <th scope="row"><label for="notes"><?php _e('Notes') ?></label></th>
		  <td><textarea name="notes" id="notes" rows="5" cols="30"></textarea></td>
	  </tr>
	  <?php do_action('closings_add_org_form'); ?>
	</tbody>
  </table>
  <?php submit_button(); ?>

</form>
</div>
