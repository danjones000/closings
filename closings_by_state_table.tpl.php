<?php foreach($closings as $state2 => $state): ?>
	<section class="Closings-state" id="Closings-state-<?php echo $state2; ?>">
		<header>
			<h2 class="Closings-state-name"><?php echo $state['name']; ?></h2>
		</header>
		<?php foreach($state['categories'] as $cat => $closings): ?>
			<table>
				<caption><?php echo $cat; ?></caption>
				<thead>
					<tr>
						<th scope="col"><?php _e('Organization Name','closings') ?></th>
						<th scope="col"><?php _e('Status','closings') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($closings as $cl): ?>
						<tr>
							<th scope="row"><?php echo $cl->org->name; ?></th>
							<td><?php if ($cl->status_code != "status_X"): echo $cl->status; endif; ?>
								<?php echo $cl->dayofweek; ?><br />
								<?php echo $cl->note; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endforeach; ?>
	</section>
<?php endforeach; ?>
