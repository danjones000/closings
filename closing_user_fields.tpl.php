<h3><?php _e('Additional Contact Details', 'closings'); ?></h3>
<table class="form-table">
	<tr>
		<th scope="row"><label for="phone"><?php _e('Phone Number'); ?></label></th>
		<td><input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /></td>
	</tr>
	<tr>
		<th scop="row"><label for="alt_name"><?php _e('Altenate Contact Name', 'closings'); ?></label></th>
		<td>
			<input type="text" name="alt_name" id="alt_name" value="<?php echo esc_attr( get_the_author_meta( 'alt_name', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description"><?php _e('Please enter the name of an alternate contact.', 'closings'); ?></span>
		</td>
	</tr>
	<tr>
		<th scop="row"><label for="alt_email"><?php _e('Altenate Contact Email', 'closings'); ?></label></th>
		<td>
			<input type="email" name="alt_email" id="alt_email" value="<?php echo esc_attr( get_the_author_meta( 'alt_email', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description"><?php _e('Please enter the email of an alternate contact.', 'closings'); ?></span>
		</td>
	</tr>
	<tr>
		<th scope="row"><label for="alt_phone"><?php _e('Alternate Phone Number', 'closings'); ?></label></th>
		<td>
			<input type="text" name="alt_phone" id="alt_phone" value="<?php echo esc_attr( get_the_author_meta( 'alt_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description"><?php _e('Please enter an alternate phone number.', 'closings'); ?></span>
		</td>
	</tr>
</table>
