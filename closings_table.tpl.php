<table>
	<thead>
		<tr>
			<th scope="col"><?php _e('Organization Name','closings') ?></th>
			<th scope="col"><?php _e('Status','closings') ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($closings as $cl): ?>
			<tr>
				<th scope="row"><?php echo $cl->org->name; ?></th>
				<td><?php if ($cl->status_code != "status_X"): echo $cl->status; endif; ?>
					<?php echo $cl->dayofweek; ?><br />
					<?php echo $cl->note; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
